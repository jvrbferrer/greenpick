# Greenpick

BE and FE 
======
* Setup
```
docker-compose build
```
* Run app
```
docker-compose up
```
* Update Gemfile with new gems and install those gems
```
docker-compose build
```


Just FE
======
* Setup
```
yarn
```
* Run app
```
yarn start
```
