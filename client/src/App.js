import React, { useEffect, useState, useLayoutEffect } from "react";
import { useDispatch } from "react-redux";
import { loadState } from "./redux/authentication";
import LandingPage from "./components/LandingPage/LandingPage";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import AboutPage from "./components/AboutPage/AboutPage";
import PrizePage from "./components/PrizePages/PrizePage";
import { Switch, Route } from "react-router-dom";
import ContactPage from "./components/ContactPage/ContactPage";
import NotFoundPage from "./components/NotFoundPage/NotFoundPage";
import CollectRewardPage from "./components/CollectRewardPage/CollectRewardPage";
import Supports from "./components/Supports/Supports";
import ScrollTo from "./components/ScrollTo/ScrollTo";
import TermsAndConditions from "./components/TermsAndConditionsPage/TermsAndConditionsPage";
import AdminProtectedPage from "./components/AdminProtectedPage/AdminProtectedPage";
import AdminPage from "./components/AdminPage/AdminPage";
import AdminCompaniesPage from "./components/AdminCompaniesPage/AdminCompaniesPage";
import AdminReviewsPage from "./components/AdminReviewsPage/AdminReviewsPage";
import AdminUsersPage from "./components/AdminUsersPage/AdminUsersPage";
import styles from "./App.module.css";
import { useTracking } from "./utils/Hooks";
import JoinUsModal from "./components/Modal/JoinUsModal";
import AdminCodesPage from "./components/AdminCodesPage/AdminCodesPage";

const App = () => {
  const [activeJoinUsModal, setActiveJoinUsModal] = useState(false);
  const [headerColor, setHeaderColor] = useState("#FFFFFF");

  //Initialize FB SDK
  const dispatch = useDispatch();
  useLayoutEffect(() => {
    dispatch(loadState());
  }, [dispatch]);
  useEffect(() => {
    window.fbAsyncInit = () => {
      window.FB.init({
        appId: "242807190120936",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v7.0",
      });
    };
    let script = document.createElement("script");
    script.src = "https://connect.facebook.net/en_US/sdk.js";
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  }, []);
  useTracking();

  return (
    <div className={"min-vh-100 " + styles.maxContainer}>
      {activeJoinUsModal && (
        <JoinUsModal closeModal={() => setActiveJoinUsModal(false)} />
      )}
      <ScrollTo scrollLocation={{ top: 0, left: 0 }} />
      <Navbar
        backgroundColor={headerColor}
        setActiveJoinUsModal={setActiveJoinUsModal}
      />
      <Switch>
        <Route
          exact
          path={[
            "/about/",
            "/about/how-does-it-work/",
            "/about/beliefs",
            "/about/team",
          ]}
        >
          <AboutPage setHeaderColor={setHeaderColor} />
        </Route>
        <Route exact path={["/reward/", "/reward/:code/"]}>
          <PrizePage setHeaderColor={setHeaderColor} />
        </Route>
        <Route exact path="/contact">
          <ContactPage setHeaderColor={setHeaderColor} />
        </Route>
        <Route exact path="/Terms&Conditions">
          <TermsAndConditions setHeaderColor={setHeaderColor} />
        </Route>
        <Route exact path="/collect-reward">
          <CollectRewardPage setHeaderColor={setHeaderColor} />
        </Route>
        <Route exact path="/Terms&Conditions">
          <TermsAndConditions setHeaderColor={setHeaderColor} />
        </Route>
        <Route exact path="/admin">
          <AdminProtectedPage>
            <AdminPage />
          </AdminProtectedPage>
        </Route>
        <Route exact path="/admin/companies">
          <AdminProtectedPage>
            <AdminCompaniesPage />
          </AdminProtectedPage>
        </Route>
        <Route exact path="/admin/reviews">
          <AdminProtectedPage>
            <AdminReviewsPage />
          </AdminProtectedPage>
        </Route>
        <Route exact path="/admin/users">
          <AdminProtectedPage>
            <AdminUsersPage />
          </AdminProtectedPage>
        </Route>
        <Route exact path="/admin/codes">
          <AdminProtectedPage>
            <AdminCodesPage />
          </AdminProtectedPage>
        </Route>
        <Route exact path="/">
          <LandingPage
            setHeaderColor={setHeaderColor}
            setActiveJoinUsModal={setActiveJoinUsModal}
          />
        </Route>
        <Route path="*">
          <NotFoundPage setHeaderColor={setHeaderColor} />
        </Route>
      </Switch>
      <Footer />
      <Supports />
    </div>
  );
};

export default App;
