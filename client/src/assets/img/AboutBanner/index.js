import BackgroundImage2x from "./BackgroundImage@2x.png";
import BackgroundImage2x_new from "./BackgroundImage@2x.webp";
import BackgroundImage from "./BackgroundImage.png";
import BackgroundImage_new from "./BackgroundImage.webp";
import BackgroundImage3x from "./BackgroundImage@3x.png";
import BackgroundImage3x_new from "./BackgroundImage@3x.webp";

export default {
  oldImages: [BackgroundImage2x, BackgroundImage, BackgroundImage3x],
  newImages: [BackgroundImage2x_new, BackgroundImage_new, BackgroundImage3x_new],
  sizes: [2878, 1439, 4317],
  format: "png",
  defaultImg: "BackgroundImage@3x"
}