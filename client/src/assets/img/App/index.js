import App from "./App.png";
import App_new from "./App.webp";
import App2x from "./App@2x.png";
import App2x_new from "./App@2x.webp";
import App3x from "./App@3x.png";
import App3x_new from "./App@3x.webp";

export default {
  oldImages: [App, App2x, App3x],
  newImages: [App_new, App2x_new, App3x_new],
  sizes: [543, 1085, 1628],
  format: "png",
  defaultImg: "App@3x"
}