import Comment1_12x from "./Comment1_1@2x.png";
import Comment1_12x_new from "./Comment1_1@2x.webp";
import Comment1_13x from "./Comment1_1@3x.png";
import Comment1_13x_new from "./Comment1_1@3x.webp";
import Comment1_1 from "./Comment1_1.png";
import Comment1_1_new from "./Comment1_1.webp";

export default {
  oldImages: [Comment1_12x, Comment1_13x, Comment1_1],
  newImages: [Comment1_12x_new, Comment1_13x_new, Comment1_1_new],
  sizes: [620, 930, 310],
  format: "png",
  defaultImg: "Comment1_1"
}