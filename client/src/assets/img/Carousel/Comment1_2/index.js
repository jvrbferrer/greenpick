import Messages from "./Messages.png";
import Messages_new from "./Messages.webp";
import Messages3x from "./Messages@3x.png";
import Messages3x_new from "./Messages@3x.webp";
import Messages2x from "./Messages@2x.png";
import Messages2x_new from "./Messages@2x.webp";

export default {
  oldImages: [Messages, Messages3x, Messages2x],
  newImages: [Messages_new, Messages3x_new, Messages2x_new],
  sizes: [211, 633, 422],
  format: "png",
  defaultImg: "Messages@2x"
}