import Comment2_1 from "./Comment2_1.png";
import Comment2_1_new from "./Comment2_1.webp";
import Comment2_12x from "./Comment2_1@2x.png";
import Comment2_12x_new from "./Comment2_1@2x.webp";
import Comment2_13x from "./Comment2_1@3x.png";
import Comment2_13x_new from "./Comment2_1@3x.webp";

export default {
  oldImages: [Comment2_1, Comment2_12x, Comment2_13x],
  newImages: [Comment2_1_new, Comment2_12x_new, Comment2_13x_new],
  sizes: [310, 620, 930],
  format: "png",
  defaultImg: "Comment2_1@3x"
}