import Comment3_1 from "./Comment3_1.png";
import Comment3_1_new from "./Comment3_1.webp";
import Comment3_12x from "./Comment3_1@2x.png";
import Comment3_12x_new from "./Comment3_1@2x.webp";
import Comment3_13x from "./Comment3_1@3x.png";
import Comment3_13x_new from "./Comment3_1@3x.webp";

export default {
  oldImages: [Comment3_1, Comment3_12x, Comment3_13x],
  newImages: [Comment3_1_new, Comment3_12x_new, Comment3_13x_new],
  sizes: [310, 620, 930],
  format: "png",
  defaultImg: "Comment3_1@3x"
}