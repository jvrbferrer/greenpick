import Comment4_13x from "./Comment4_1@3x.png";
import Comment4_13x_new from "./Comment4_1@3x.webp";
import Comment4_1 from "./Comment4_1.png";
import Comment4_1_new from "./Comment4_1.webp";
import Comment4_12x from "./Comment4_1@2x.png";
import Comment4_12x_new from "./Comment4_1@2x.webp";

export default {
  oldImages: [Comment4_13x, Comment4_1, Comment4_12x],
  newImages: [Comment4_13x_new, Comment4_1_new, Comment4_12x_new],
  sizes: [930, 310, 620],
  format: "png",
  defaultImg: "Comment4_1@2x"
}