import Comment5_12x from "./Comment5_1@2x.png";
import Comment5_12x_new from "./Comment5_1@2x.webp";
import Comment5_1 from "./Comment5_1.png";
import Comment5_1_new from "./Comment5_1.webp";
import Comment5_23x from "./Comment5_2@3x.png";
import Comment5_23x_new from "./Comment5_2@3x.webp";

export default {
  oldImages: [Comment5_12x, Comment5_1, Comment5_23x],
  newImages: [Comment5_12x_new, Comment5_1_new, Comment5_23x_new],
  sizes: [620, 310, 930],
  format: "png",
  defaultImg: "Comment5_2@3x"
}