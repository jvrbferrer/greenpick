import Box3x from "./Box@3x.png";
import Box3x_new from "./Box@3x.webp";
import Box from "./Box.png";
import Box_new from "./Box.webp";
import Box2x from "./Box@2x.png";
import Box2x_new from "./Box@2x.webp";

export default {
  oldImages: [Box3x, Box, Box2x],
  newImages: [Box3x_new, Box_new, Box2x_new],
  sizes: [742, 248, 495],
  format: "png",
  defaultImg: "Box@2x"
}