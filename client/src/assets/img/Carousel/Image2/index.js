import GoodBag from "./GoodBag.png";
import GoodBag_new from "./GoodBag.webp";
import GoodBag2x from "./GoodBag@2x.png";
import GoodBag2x_new from "./GoodBag@2x.webp";
import GoodBag3x from "./GoodBag@3x.png";
import GoodBag3x_new from "./GoodBag@3x.webp";

export default {
  oldImages: [GoodBag, GoodBag2x, GoodBag3x],
  newImages: [GoodBag_new, GoodBag2x_new, GoodBag3x_new],
  sizes: [277, 552, 827],
  format: "png",
  defaultImg: "GoodBag@3x"
}