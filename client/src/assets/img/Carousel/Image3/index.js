import FastFoodTrash3x from "./FastFoodTrash@3x.png";
import FastFoodTrash3x_new from "./FastFoodTrash@3x.webp";
import FastFoodTrash from "./FastFoodTrash.png";
import FastFoodTrash_new from "./FastFoodTrash.webp";
import FastFoodTrash2x from "./FastFoodTrash@2x.png";
import FastFoodTrash2x_new from "./FastFoodTrash@2x.webp";

export default {
  oldImages: [FastFoodTrash3x, FastFoodTrash, FastFoodTrash2x],
  newImages: [FastFoodTrash3x_new, FastFoodTrash_new, FastFoodTrash2x_new],
  sizes: [827, 277, 552],
  format: "png",
  defaultImg: "FastFoodTrash@2x"
}