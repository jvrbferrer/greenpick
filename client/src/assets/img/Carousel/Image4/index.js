import Jars3x from "./Jars@3x.png";
import Jars3x_new from "./Jars@3x.webp";
import Jars from "./Jars.png";
import Jars_new from "./Jars.webp";
import Jars2x from "./Jars@2x.png";
import Jars2x_new from "./Jars@2x.webp";

export default {
  oldImages: [Jars3x, Jars, Jars2x],
  newImages: [Jars3x_new, Jars_new, Jars2x_new],
  sizes: [827, 277, 552],
  format: "png",
  defaultImg: "Jars@2x"
}