import BabyDiapers3x from "./BabyDiapers@3x.png";
import BabyDiapers3x_new from "./BabyDiapers@3x.webp";
import BabyDiapers2x from "./BabyDiapers@2x.png";
import BabyDiapers2x_new from "./BabyDiapers@2x.webp";
import BabyDiapers from "./BabyDiapers.png";
import BabyDiapers_new from "./BabyDiapers.webp";

export default {
  oldImages: [BabyDiapers3x, BabyDiapers2x, BabyDiapers],
  newImages: [BabyDiapers3x_new, BabyDiapers2x_new, BabyDiapers_new],
  sizes: [827, 552, 277],
  format: "png",
  defaultImg: "BabyDiapers"
}