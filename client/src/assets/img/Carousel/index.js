import comment1_1 from "./Comment1_1";
import comment1_2 from "./Comment1_2";
import comment2_1 from "./Comment2_1";
import comment3_1 from "./Comment3_1";
import comment3_2 from "./Comment3_2";
import comment4_1 from "./Comment4_1";
import comment5_1 from "./Comment5_1";
import Image1 from "./Image1";
import Image2 from "./Image2";
import Image3 from "./Image3";
import Image4 from "./Image4";
import Image5 from "./Image5";
import Oval from "./Oval.svg";
import Oval2 from "./Oval2.svg";

export {
  comment1_1,
  comment1_2,
  comment2_1,
  comment3_2,
  comment3_1,
  comment4_1,
  comment5_1,
  Image1,
  Image2,
  Image3,
  Image4,
  Image5,
  Oval,
  Oval2
};
