import CicleOfGreenpickMB from "./CicleOfGreenpickMB.jpg";
import CicleOfGreenpickMB_new from "./CicleOfGreenpickMB.webp";
import CicleOfGreenpick from "./CicleOfGreenpick.jpg";
import CicleOfGreenpick_new from "./CicleOfGreenpick.webp";

export default {
  oldImages: [CicleOfGreenpickMB, CicleOfGreenpick],
  newImages: [CicleOfGreenpickMB_new, CicleOfGreenpick_new],
  sizes: [1610, 2395],
  format: "jpg",
  defaultImg: "CicleOfGreenpick"
}