import Forest from "./Forest.png";
import Forest_new from "./Forest.webp";
import Forest3x from "./Forest@3x.png";
import Forest3x_new from "./Forest@3x.webp";
import Forest2x from "./Forest@2x.png";
import Forest2x_new from "./Forest@2x.webp";

export default {
  oldImages: [Forest, Forest3x, Forest2x],
  newImages: [Forest_new, Forest3x_new, Forest2x_new],
  sizes: [623, 1869, 1246],
  format: "png",
  defaultImg: "Forest@2x"
}