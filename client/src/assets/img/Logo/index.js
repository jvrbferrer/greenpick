import logo2x from "./logo@2x.png";
import logo2x_new from "./logo@2x.webp";
import logo3x from "./logo@3x.png";
import logo3x_new from "./logo@3x.webp";
import logo from "./logo.png";
import logo_new from "./logo.webp";

export default {
  oldImages: [logo2x, logo3x, logo],
  newImages: [logo2x_new, logo3x_new, logo_new],
  sizes: [64, 96, 32],
  format: "png",
  defaultImg: "logo"
}