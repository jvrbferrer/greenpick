import ReviewsPage from "./ReviewsPage.png";
import ReviewsPage_new from "./ReviewsPage.webp";
import ReviewsPage2x from "./ReviewsPage@2x.png";
import ReviewsPage2x_new from "./ReviewsPage@2x.webp";
import ReviewsPage3x from "./ReviewsPage@3x.png";
import ReviewsPage3x_new from "./ReviewsPage@3x.webp";

export default {
  oldImages: [ReviewsPage, ReviewsPage2x, ReviewsPage3x],
  newImages: [ReviewsPage_new, ReviewsPage2x_new, ReviewsPage3x_new],
  sizes: [592, 1184, 1776],
  format: "png",
  defaultImg: "ReviewsPage@3x"
}