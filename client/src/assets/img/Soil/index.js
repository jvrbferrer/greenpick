import Soil3x from "./Soil@3x.png";
import Soil3x_new from "./Soil@3x.webp";
import Soil2x from "./Soil@2x.png";
import Soil2x_new from "./Soil@2x.webp";
import Soil from "./Soil.png";
import Soil_new from "./Soil.webp";

export default {
  oldImages: [Soil3x, Soil2x, Soil],
  newImages: [Soil3x_new, Soil2x_new, Soil_new],
  sizes: [1722, 1148, 574],
  format: "png",
  defaultImg: "Soil"
}