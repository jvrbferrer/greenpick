import iapmei2x from "./iapmei@2x.png";
import iapmei2x_new from "./iapmei@2x.webp";
import iapmei from "./iapmei.png";
import iapmei_new from "./iapmei.webp";
import iapmei3x from "./iapmei@3x.png";
import iapmei3x_new from "./iapmei@3x.webp";

export default {
  oldImages: [iapmei2x, iapmei, iapmei3x],
  newImages: [iapmei2x_new, iapmei_new, iapmei3x_new],
  sizes: [218, 109, 327],
  format: "png",
  defaultImg: "iapmei@3x"
}