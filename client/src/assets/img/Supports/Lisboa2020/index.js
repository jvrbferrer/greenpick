import Lisboa20203x from "./Lisboa2020@3x.png";
import Lisboa20203x_new from "./Lisboa2020@3x.webp";
import Lisboa20202x from "./Lisboa2020@2x.png";
import Lisboa20202x_new from "./Lisboa2020@2x.webp";
import Lisboa2020 from "./Lisboa2020.png";
import Lisboa2020_new from "./Lisboa2020.webp";

export default {
  oldImages: [Lisboa20203x, Lisboa20202x, Lisboa2020],
  newImages: [Lisboa20203x_new, Lisboa20202x_new, Lisboa2020_new],
  sizes: [306, 204, 102],
  format: "png",
  defaultImg: "Lisboa2020"
}