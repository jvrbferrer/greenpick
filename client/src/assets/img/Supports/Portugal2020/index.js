import Portugal2020 from "./Portugal2020.png";
import Portugal2020_new from "./Portugal2020.webp";
import Portugal20202x from "./Portugal2020@2x.png";
import Portugal20202x_new from "./Portugal2020@2x.webp";
import Portugal20203x from "./Portugal2020@3x.png";
import Portugal20203x_new from "./Portugal2020@3x.webp";

export default {
  oldImages: [Portugal2020, Portugal20202x, Portugal20203x],
  newImages: [Portugal2020_new, Portugal20202x_new, Portugal20203x_new],
  sizes: [102, 204, 306],
  format: "png",
  defaultImg: "Portugal2020@3x"
}