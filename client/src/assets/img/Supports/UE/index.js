import UE3x from "./UE@3x.png";
import UE3x_new from "./UE@3x.webp";
import UE from "./UE.png";
import UE_new from "./UE.webp";
import UE2x from "./UE@2x.png";
import UE2x_new from "./UE@2x.webp";

export default {
  oldImages: [UE3x, UE, UE2x],
  newImages: [UE3x_new, UE_new, UE2x_new],
  sizes: [462, 154, 308],
  format: "png",
  defaultImg: "UE@2x"
}