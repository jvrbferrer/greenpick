import AfonsoPinheiro2x from "./Afonso-Pinheiro@2x.jpg";
import AfonsoPinheiro2x_new from "./Afonso-Pinheiro@2x.webp";
import AfonsoPinheiro3x from "./Afonso-Pinheiro@3x.jpg";
import AfonsoPinheiro3x_new from "./Afonso-Pinheiro@3x.webp";
import AfonsoPinheiro from "./Afonso-Pinheiro.jpg";
import AfonsoPinheiro_new from "./Afonso-Pinheiro.webp";

export default {
  oldImages: [AfonsoPinheiro2x, AfonsoPinheiro3x, AfonsoPinheiro],
  newImages: [AfonsoPinheiro2x_new, AfonsoPinheiro3x_new, AfonsoPinheiro_new],
  sizes: [713, 1080, 357],
  format: "jpg",
  defaultImg: "Afonso-Pinheiro"
}