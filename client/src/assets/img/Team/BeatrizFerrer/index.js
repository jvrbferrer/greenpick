import BeatrizFerrer from "./Beatriz-Ferrer.jpg";
import BeatrizFerrer_new from "./Beatriz-Ferrer.webp";
import BeatrizFerrer3x from "./Beatriz-Ferrer@3x.jpg";
import BeatrizFerrer3x_new from "./Beatriz-Ferrer@3x.webp";
import BeatrizFerrer2x from "./Beatriz-Ferrer@2x.jpg";
import BeatrizFerrer2x_new from "./Beatriz-Ferrer@2x.webp";

export default {
  oldImages: [BeatrizFerrer, BeatrizFerrer3x, BeatrizFerrer2x],
  newImages: [BeatrizFerrer_new, BeatrizFerrer3x_new, BeatrizFerrer2x_new],
  sizes: [258, 780, 515],
  format: "jpg",
  defaultImg: "Beatriz-Ferrer@2x"
}