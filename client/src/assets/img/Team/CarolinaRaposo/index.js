import CarolinaRaposo from "./Carolina-Raposo.png";
import CarolinaRaposo_new from "./Carolina-Raposo.webp";
import CarolinaRaposo3x from "./Carolina-Raposo@3x.png";
import CarolinaRaposo3x_new from "./Carolina-Raposo@3x.webp";
import CarolinaRaposo2x from "./Carolina-Raposo@2x.png";
import CarolinaRaposo2x_new from "./Carolina-Raposo@2x.webp";

export default {
  oldImages: [CarolinaRaposo, CarolinaRaposo3x, CarolinaRaposo2x],
  newImages: [CarolinaRaposo_new, CarolinaRaposo3x_new, CarolinaRaposo2x_new],
  sizes: [173, 523, 345],
  format: "png",
  defaultImg: "Carolina-Raposo@2x"
}