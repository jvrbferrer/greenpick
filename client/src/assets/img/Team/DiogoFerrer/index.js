import DiogoFerrer from "./Diogo-Ferrer.jpg";
import DiogoFerrer_new from "./Diogo-Ferrer.webp";
import DiogoFerrer2x from "./Diogo-Ferrer@2x.jpg";
import DiogoFerrer2x_new from "./Diogo-Ferrer@2x.webp";
import DiogoFerrer3x from "./Diogo-Ferrer@3x.jpg";
import DiogoFerrer3x_new from "./Diogo-Ferrer@3x.webp";

export default {
  oldImages: [DiogoFerrer, DiogoFerrer2x, DiogoFerrer3x],
  newImages: [DiogoFerrer_new, DiogoFerrer2x_new, DiogoFerrer3x_new],
  sizes: [451, 901, 1365],
  format: "jpg",
  defaultImg: "Diogo-Ferrer@3x"
}