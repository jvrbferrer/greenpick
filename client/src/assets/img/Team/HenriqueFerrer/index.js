import HenriqueFerrer2x from "./Henrique-Ferrer@2x.png";
import HenriqueFerrer2x_new from "./Henrique-Ferrer@2x.webp";
import HenriqueFerrer3x from "./Henrique-Ferrer@3x.png";
import HenriqueFerrer3x_new from "./Henrique-Ferrer@3x.webp";
import HenriqueFerrer from "./Henrique-Ferrer.png";
import HenriqueFerrer_new from "./Henrique-Ferrer.webp";

export default {
  oldImages: [HenriqueFerrer2x, HenriqueFerrer3x, HenriqueFerrer],
  newImages: [HenriqueFerrer2x_new, HenriqueFerrer3x_new, HenriqueFerrer_new],
  sizes: [733, 1110, 367],
  format: "png",
  defaultImg: "Henrique-Ferrer"
}