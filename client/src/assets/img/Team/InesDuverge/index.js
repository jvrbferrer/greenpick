import InesDuverge from "./Ines-Duverge.jpg";
import InesDuverge_new from "./Ines-Duverge.webp";
import InesDuverge3x from "./Ines-Duverge@3x.jpg";
import InesDuverge3x_new from "./Ines-Duverge@3x.webp";
import InesDuverge2x from "./Ines-Duverge@2x.jpg";
import InesDuverge2x_new from "./Ines-Duverge@2x.webp";

export default {
  oldImages: [InesDuverge, InesDuverge3x, InesDuverge2x],
  newImages: [InesDuverge_new, InesDuverge3x_new, InesDuverge2x_new],
  sizes: [147, 446, 294],
  format: "jpg",
  defaultImg: "Ines-Duverge@2x"
}