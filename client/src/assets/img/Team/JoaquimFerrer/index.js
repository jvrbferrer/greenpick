import JoaquimFerrer from "./Joaquim-Ferrer.jpg";
import JoaquimFerrer_new from "./Joaquim-Ferrer.webp";
import JoaquimFerrer3x from "./Joaquim-Ferrer@3x.jpg";
import JoaquimFerrer3x_new from "./Joaquim-Ferrer@3x.webp";
import JoaquimFerrer2x from "./Joaquim-Ferrer@2x.jpg";
import JoaquimFerrer2x_new from "./Joaquim-Ferrer@2x.webp";

export default {
  oldImages: [JoaquimFerrer, JoaquimFerrer3x, JoaquimFerrer2x],
  newImages: [JoaquimFerrer_new, JoaquimFerrer3x_new, JoaquimFerrer2x_new],
  sizes: [337, 1021, 674],
  format: "jpg",
  defaultImg: "Joaquim-Ferrer@2x"
}