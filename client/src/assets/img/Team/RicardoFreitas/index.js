import RicardoFreitas from "./Ricardo-Freitas.png";
import RicardoFreitas_new from "./Ricardo-Freitas.webp";
import RicardoFreitas3x from "./Ricardo-Freitas@3x.png";
import RicardoFreitas3x_new from "./Ricardo-Freitas@3x.webp";
import RicardoFreitas2x from "./Ricardo-Freitas@2x.png";
import RicardoFreitas2x_new from "./Ricardo-Freitas@2x.webp";

export default {
  oldImages: [RicardoFreitas, RicardoFreitas3x, RicardoFreitas2x],
  newImages: [RicardoFreitas_new, RicardoFreitas3x_new, RicardoFreitas2x_new],
  sizes: [149, 450, 297],
  format: "png",
  defaultImg: "Ricardo-Freitas@2x"
}