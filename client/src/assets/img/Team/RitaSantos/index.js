import RitaSantos3x from "./RitaSantos@3x.jpg";
import RitaSantos3x_new from "./RitaSantos@3x.webp";
import RitaSantos from "./RitaSantos.jpg";
import RitaSantos_new from "./RitaSantos.webp";
import RitaSantos2x from "./RitaSantos@2x.jpg";
import RitaSantos2x_new from "./RitaSantos@2x.webp";

export default {
  oldImages: [RitaSantos3x, RitaSantos, RitaSantos2x],
  newImages: [RitaSantos3x_new, RitaSantos_new, RitaSantos2x_new],
  sizes: [850, 281, 561],
  format: "jpg",
  defaultImg: "RitaSantos@2x"
}