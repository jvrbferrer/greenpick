import React from "react";
import styles from "./AboutPage.module.css";
import { Switch, Route, NavLink } from "react-router-dom";
import WhatAreWeUpTo from "./WhatAreWeUpTo";
import HowDoestItWork from "./HowDoesItWork";
import OurBeliefs from "./OurBeliefs";
import Team from "./Team";
import ForestBar from "../ForestBar/ForestBar";
import { useChangeNavbarColor } from "../../utils/Hooks";
import Image from "../Image/Image";
import AboutBannerObj from "../../assets/img/AboutBanner";
import MaxWidthDiv from "../MaxWidthDiv/MaxWidthDiv";

const AboutPage = ({ setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#FFFFFF"));
  return (
    <>
      <div className={styles.bannerContainer}>
        <Image
          imgObject={AboutBannerObj}
          alt="Plant on top of cardbox"
          className={styles.banner}
        />
        <p className={styles.aboutUsText}>About us</p>
      </div>
      <div className={styles.mainContainer}>
        <div className={styles.nav}>
          <nav className={styles.aboutNav}>
            <NavLink
              exact
              to="/about/"
              activeClassName={styles.activeLink}
              className={styles.link}
            >
              What are we up to?
            </NavLink>
            <NavLink
              exact
              to="/about/how-does-it-work"
              activeClassName={styles.activeLink}
              className={styles.link}
            >
              How does it work?
            </NavLink>
            <NavLink
              exact
              to="/about/beliefs"
              activeClassName={styles.activeLink}
              className={styles.link}
            >
              Our beliefs
            </NavLink>
            <NavLink
              exact
              to="/about/team"
              activeClassName={styles.activeLink}
              className={styles.link}
            >
              The team
            </NavLink>
          </nav>
        </div>
        <MaxWidthDiv maxWidth="800px" className={styles.content}>
          <Switch>
            <Route exact path="/about">
              <WhatAreWeUpTo />
            </Route>
            <Route exact path="/about/how-does-it-work">
              <HowDoestItWork />
            </Route>
            <Route exact path="/about/beliefs">
              <OurBeliefs />
            </Route>
            <Route exact path="/about/team">
              <Team />
            </Route>
          </Switch>
        </MaxWidthDiv>
      </div>
      <ForestBar />
    </>
  );
};

export default AboutPage;
