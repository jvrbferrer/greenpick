import React from "react";
import styles from "./AboutPage.module.css";
import Card from "./Card";

const CardImgText = props => {
  return (
    <Card
      className={
        "d-flex flex-row align-items-center " +
        styles.cardImgText +
        " " +
        props.className
      }
    >
      <img
        alt=""
        src={props.imgSrc}
        className={"mx-4 " + styles.cardImgTextImg}
      />
      <p className={styles.cardImgTextText}>{props.text}</p>
    </Card>
  );
};

export default CardImgText;
