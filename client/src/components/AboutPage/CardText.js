import React from "react";
import styles from "./AboutPage.module.css";
import Card from "./Card";

const CardText = props => {
  return (
    <Card
      className={
        "d-flex align-items-center justify-content-center " + styles.cardText
      }
    >
      <p className={styles.cardTextText + " " + styles.green}>{props.text}</p>
    </Card>
  );
};

export default CardText;
