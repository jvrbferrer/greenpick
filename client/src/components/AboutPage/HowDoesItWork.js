import React from "react";
import styles from "./AboutPage.module.css";
import CardImgText from "./CardImgText";
import genericRatingImg from "../../assets/img/generic-rating.svg";
import genericMagnifyingGlassImg from "../../assets/img/generic-magnifying-glass.svg";
import genericTreeBalanceImg from "../../assets/img/generic-tree-balance.svg";
import genericTreeImg from "../../assets/img/generic-tree.svg";
import genericChatBalloonsImg from "../../assets/img/generic-chat-balloons.svg";
import genericSpeakerImg from "../../assets/img/generic-speaker.svg";
import genericSurveyImg from "../../assets/img/generic-survey.svg";
import genericHandPlantImg from "../../assets/img/generic-hand-plant.svg";

const HowDoesItWork = () => {
  return (
    <>
      <h1 className={"mt-1 " + styles.title}>
        How does it work for consumers?
      </h1>
      <CardImgText
        imgSrc={genericRatingImg}
        text="Review products & services regarding environmental sustainability"
      />
      <CardImgText
        className="mt-4"
        imgSrc={genericMagnifyingGlassImg}
        text="Search for environmentally sustainable brands & products"
      />
      <CardImgText
        className="mt-4"
        imgSrc={genericTreeBalanceImg}
        text="Compare brands & products regarding environmental sustainability"
      />
      <CardImgText
        className="mt-4"
        imgSrc={genericTreeImg}
        text="Receive eco-rewards to reduce environmental footprint"
      />

      <p className={"mt-5 " + styles.title + " " + styles.newSection}>
        How does it work for brands?
      </p>
      <CardImgText
        className="mb-5"
        imgSrc={genericChatBalloonsImg}
        text="Provide feedback to customer reviews"
      />
      <CardImgText
        className="mb-5"
        imgSrc={genericSpeakerImg}
        text="Share your sustainable products & services"
      />
      <CardImgText
        className="mb-5"
        imgSrc={genericSurveyImg}
        text="Create surveys and brand awareness for target audience"
      />
      <CardImgText
        className="mb-5"
        imgSrc={genericMagnifyingGlassImg}
        text="Search for more environmentally sustainable suppliers"
      />
      <CardImgText
        className="mb-5"
        imgSrc={genericHandPlantImg}
        text="Support environmental causes & reduce environmental footprint"
      />
    </>
  );
};

export default HowDoesItWork;
