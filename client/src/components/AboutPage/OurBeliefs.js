import React from "react";
import styles from "./AboutPage.module.css";
import CardText from "./CardText";

const OurBeliefs = () => {
  return (
    <>
      <h1 className={styles.title}>What’s our vision & mission?</h1>
      <h2 className={styles.subTitle}>
        To promote environmental sustainability through eco-awareness & consumer
        empowerment.
      </h2>
      <p className={styles.text}>
        Our mission is to help change the operation of our consumer society
        towards environmental sustainability by connecting individuals and
        businesses to share information in a transparent community-based online
        platform. Our vision is that we all can play a role in reversing
        environmental negative trends and in creating positive change through
        the products and services we choose to review and buy.
      </p>
      <div className={"my-5 " + styles.missionCardContainer}>
        <CardText text="Responsibility" />
        <CardText text="Sustainability" />
        <CardText text="Transparency" />
        <CardText text="Trustworthiness" />
      </div>
      <p className={styles.text}>
        Greenpick’s goal is to help build environmentally sustainable
        communities by allowing consumers and businesses to make informed
        choices towards responsible consumption and production.
      </p>
      <p className={styles.text}>
        Our team is developing an online platform able to solve the major
        problems eco-conscious consumers face today – the lack of information,
        of trust and individual reach – and also the problems related to the
        lack of transparency, responsibility and eco-awareness that affect many
        businesses today.
      </p>
      <p className={styles.text}>
        Greenpick is working on an open-to-all eco-conscious reviews platform,
        in which both individuals and companies can share their environmental
        concerns, based on sustainability, trustworthiness & transparency
        values.
      </p>
    </>
  );
};

export default OurBeliefs;
