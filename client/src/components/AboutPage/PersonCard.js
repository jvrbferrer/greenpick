import React from "react";
import styles from "./AboutPage.module.css";
import Card from "./Card";
import Image from "../Image/Image";

const PersonCard = ({ className, name, imgObject, title }) => {
  return (
    <Card
      className={
        "d-flex flex-row align-items-center px-4 " +
        styles.personCard +
        " " +
        className
      }
    >
      <div className={"mr-4 " + styles.imgContainer}>
        <Image
          alt={`${name}'s pretty face`}
          imgObject={imgObject}
          className={styles.personCardImg}
        />
      </div>
      <div className="d-flex flex-column">
        <p className={"mb-0 " + styles.text + " " + styles.green}>{name}</p>
        <p className={"mb-1 " + styles.text}>{title}</p>
      </div>
    </Card>
  );
};

export default PersonCard;
