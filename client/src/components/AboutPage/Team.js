import React from "react";
import styles from "./AboutPage.module.css";
import PersonCard from "./PersonCard";
import AfonsoPic from "../../assets/img/Team/AfonsoPinheiro";
import BeatrizPic from "../../assets/img/Team/BeatrizFerrer";
import CarolinaPic from "../../assets/img/Team/CarolinaRaposo";
import DiogoPic from "../../assets/img/Team/DiogoFerrer";
import HenriquePic from "../../assets/img/Team/HenriqueFerrer";
import InesPic from "../../assets/img/Team/InesDuverge";
import JoaquimPic from "../../assets/img/Team/JoaquimFerrer";
import RitaPic from "../../assets/img/Team/RitaSantos";

const Team = () => {
  return (
    <>
      <h1 className={styles.title}>Who is behind Greenpick?</h1>
      <p className={styles.text}>
        Greenpick is a mission-driven company working on the launch of the
        world’s first online platform for people to review, share and discover
        the sustainability of products and brands. Run by a dedicated team which
        believes that everyone must assume personal responsibility and be
        empowered to take charge of the environment, Greenpick is enlisting
        people and businesses all around the world in the cause of environmental
        sustainability.
      </p>
      <p className={styles.title + " " + styles.newSection}>Our team</p>
      <div className={styles.peopleContainer}>
        <PersonCard imgObject={BeatrizPic} name="Beatriz Ferrer" title="CEO" />
        <PersonCard imgObject={JoaquimPic} name="Joaquim Ferrer" title="CTO" />
        <PersonCard
          imgObject={InesPic}
          name="Inês Duvergê"
          title="Product & brand designer"
        />
        <PersonCard
          imgObject={RitaPic}
          name="Rita Santos"
          title="Marketing manager"
        />
        <PersonCard
          imgObject={DiogoPic}
          name="Diogo Ferrer"
          title="Software engineer"
        />
        <PersonCard
          imgObject={HenriquePic}
          name="Henrique Ferrer"
          title="Software engineer"
        />
        <PersonCard
          imgObject={AfonsoPic}
          name="Afonso Pinheiro"
          title="Advisor"
        />
        <PersonCard
          imgObject={CarolinaPic}
          name="Carolina Raposo"
          title="Advisor"
        />
      </div>
    </>
  );
};

export default Team;
