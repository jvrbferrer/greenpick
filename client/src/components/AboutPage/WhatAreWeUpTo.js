import React from "react";
import { Link } from "react-router-dom";
import styles from "./AboutPage.module.css";
import cicleOfGreenpick from "../../assets/img/CicleOfGreenpick/CicleOfGreenpick.jpg";
import cicleOfGreenpickMB from "../../assets/img/CicleOfGreenpick/CicleOfGreenpickMB.jpg";
const WhatAreWeUpTp = () => {
  return (
    <>
      <h1 className={styles.title}>What are we up to?</h1>
      <h2 className={styles.subTitle}>
        Greenpick is developing an app to give people the power to praise or
        penalize the sustainability of products and services by taking into
        consideration common sense eco-friendly practices.
      </h2>
      <p className={styles.text}>
        With a unique database of worldwide reviews on the sustainability of
        products, Greenpick encloses the potential to rapidly revolutionize the
        way people and businesses look at the products they consume and sell,
        promoting the necessary change.
      </p>
      <p className={styles.text}>
        By giving communities a voice and space to share information and
        generate support for the adoption of eco-friendly behaviours and
        practices, Greenpick presents a reasonable and tangible solution to
        address sustainability problems at a global scale, starting from our
        homes.
      </p>
      <p className={styles.text + " " + styles.green}>
        <Link
          to={{ hash: "subscribeForm" }}
          className={styles.text + " " + styles.green}
          onClick={() => {
            document.getElementById("subscribeForm").focus();
          }}
        >
          <u>Join us</u>
        </Link>{" "}
        in this journey for the preservation of the future of our planet!
      </p>
      {/* <img
        className={styles.whatImg}
        srcSet={`
          ${cicleOfGreenpickMB} 1610w,
          ${cicleOfGreenpick} 2395w
        `}
        src={cicleOfGreenpickMB}
        alt=""
      /> */}
      <picture>
        <source media="(min-width: 1200px)" srcSet={cicleOfGreenpick} />
        <img src={cicleOfGreenpickMB} alt="" className={styles.whatImg} />
      </picture>
      <p className={styles.title + " " + styles.newSection}>
        Why join Greenpick?
      </p>
      <h2 className={styles.subTitle}>
        Greenpick empowers you to be a key player of the enviromental movement
      </h2>
      <p className={styles.text}>
        Changing the operation of our consumer society towards environmental
        sustainability is a great challenge. Our platform trusts you to help
        lead the way.
      </p>
      <p className={styles.text}>
        In your everyday life you surely are confronted with unacceptable
        wastage of resources and energy in almost everything you consume and do.
      </p>
      <p className={styles.text}>
        What if you could show a red card to the brands?
      </p>
      <p className={styles.text}>
        Fortunately, a growing number of brands are rethinking their products
        and services, and you have certainly come across with impressive new
        designs, materials and technologies that meet stricter sustainability
        goals.
      </p>
      <p className={styles.text}>
        What if you could applaud those efforts and set them as an example for
        others?
      </p>
      <p className={styles.text}>With Greenpick you can!</p>
      <p className={styles.text}>
        By joining Greenpick you are not only contributing to create awareness
        with your reviews, but also supporting environmental causes with the
        rewards you collect – from reforestation, to recovering plastic from
        oceans, to beaches & forests cleaning, to other environmental projects
        you may care about.
      </p>
      <p className={styles.text}>
        Your reviews are crucial to bring this platform to life. Together, we
        will make a decisive effort towards the sustainability of our planet.
      </p>

      <p className={styles.text + " " + styles.green}>
        <Link
          to={{ hash: "subscribeForm" }}
          className={styles.text + " " + styles.green}
          onClick={() => {
            document.getElementById("subscribeForm").focus();
          }}
        >
          <u>Join us</u>
        </Link>{" "}
        in this journey for the preservation of the future of our planet!
      </p>
    </>
  );
};

export default WhatAreWeUpTp;
