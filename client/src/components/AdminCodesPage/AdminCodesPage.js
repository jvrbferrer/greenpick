import React, { useEffect, useState } from "react";
import styles from "./AdminCodesPage.module.css";
import { createCode as createCodeAPI, getCodes } from "../../utils/APICalls";
import PrimaryButton from "../GPButton/PrimaryButton";

const AdminCodesPage = () => {
  const [codes, setCodes] = useState([]);
  const [newCode, setNewCode] = useState(null);

  useEffect(() => {
    getCodes()
      .then(response => {
        if (response._ok) {
          setCodes(response.prizeCodes);
        }
      })
      .catch(error => console.log(error));
  }, [setCodes]);

  const createCode = () => {
    createCodeAPI()
      .then(response => {
        if (response._ok) {
          setNewCode(response.prizeCode);
          setCodes([response.prizeCode, ...codes]);
        }
      })
      .catch(error => console.log(error));
  };

  return (
    <div className={styles.container}>
      <div className={styles.newCodeForm}>
        <PrimaryButton onClick={createCode} className={styles.newCodeButton}>
          Create new code
        </PrimaryButton>
        {newCode && <p className={styles.newCode}>{newCode.code_string}</p>}
      </div>
      <div className={styles.usedCodesContainer}>
        <h3>Submitted codes</h3>
        {codes
          .filter(code => code.used)
          .map(code => {
            return (
              <div key={code.id}>
                <p>
                  {code.code_string} : {code.updated_at}
                </p>
              </div>
            );
          })}
      </div>
      <div className={styles.notUsedCodesContainer}>
        <h3>Unsubmitted codes</h3>
        {codes
          .filter(code => !code.used)
          .map(code => {
            return (
              <div key={code.id}>
                <p>
                  {code.code_string} : {code.updated_at}
                </p>
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default AdminCodesPage;
