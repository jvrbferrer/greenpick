import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import styles from "./AdminCompaniesPage.module.css";
import SearchCompanyForm from "./SearchCompanyForm";
import CreateCompanyForm from "./CreateCompanyForm";
import CompanyPage from "./CompanyPage";

const AdminCompaniesPage = () => {
  const [selectedCompany, setSelectedCompany] = useState(null);
  const [createCompanyName, setCreateCompanyName] = useState("");
  const [editCompany, setEditCompany] = useState(false);
  return (
    <>
      <Container fluid className={styles.container}>
        <p className={styles.header}>Companies</p>
        <Row>
          <Col xs={12} md={6}>
            <SearchCompanyForm
              setCreateCompanyName={(val) => {
                setCreateCompanyName(val);
                setSelectedCompany(null);
              }}
              setSelectedCompany={(val) => {
                setSelectedCompany(val);
                setCreateCompanyName("");
              }}
            />
          </Col>
          <Col xs={12} md={6}>
            {(createCompanyName !== "" || editCompany) && (
              <div className={styles.createForm}>
                <p className={styles.createCompanyHeader}>Create company</p>
                <CreateCompanyForm
                  name={createCompanyName}
                  key={createCompanyName}
                  editCompany={editCompany ? selectedCompany : null}
                />
              </div>
            )}
          </Col>
        </Row>
        <Row>
          <Col xs={12} className={styles.companyPage}>
            {selectedCompany && (
              <CompanyPage
                {...selectedCompany}
                onDelete={() => setSelectedCompany(null)}
                setEditCompany={() => setEditCompany(true)}
                key={selectedCompany.id}
              />
            )}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AdminCompaniesPage;
