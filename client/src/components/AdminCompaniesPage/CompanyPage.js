import React, { useState } from "react";
import SpecialButton from "../GPButton/SpecialButton";
import CompaniesEmail from "../emails/CompaniesEmail";
import Review from "./Review";
import CountryCodes from "country-codes-list";
import {
  getCompanyReviews as getCompanyReviewsAPI,
  deleteCompany as deleteCompanyAPI,
} from "../../utils/APICalls";
import styles from "./CompanyPage.module.css";

const CompanyPage = ({
  name,
  logoUrl,
  countryCode,
  verified,
  id,
  website,
  className,
  onDelete,
  setEditCompany,
}) => {
  const [renderEmail, setRenderEmail] = useState("");
  const [newWindow, setNewWindow] = useState(null);
  const [reviews, setReviews] = useState([]);
  const [deleted, setDeleted] = useState(false);
  const countryCodesObj = CountryCodes.customList(
    "countryCode",
    "{countryNameEn}"
  );

  const generateCompanyEmail = async () => {
    setNewWindow(window.open("", `${name} Email`));
    setRenderEmail("companiesReviews");
  };

  const getCompanyReviews = () => {
    getCompanyReviewsAPI({ companyId: id })
      .then((res) => {
        if (res._ok) {
          setReviews(res.reviews);
        } else {
          console.log(res);
        }
      })
      .catch((error) => console.log(error));
  };

  const deleteCompany = () => {
    deleteCompanyAPI({ companyId: id })
      .then((res) => {
        if (res._ok) {
          setDeleted(true);
          onDelete();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const reviewsButton =
    reviews.length > 0 ? (
      <SpecialButton
        onClick={generateCompanyEmail}
        className={styles.reviewsButton}
      >
        Generate reviews email
      </SpecialButton>
    ) : (
      <SpecialButton
        onClick={getCompanyReviews}
        className={styles.reviewsButton}
      >
        Get reviews
      </SpecialButton>
    );

  if (deleted) {
    return <p>Deleted</p>;
  }
  return (
    <>
      {renderEmail === "companiesReviews" && (
        <CompaniesEmail
          newWindow={newWindow}
          name={name}
          logoUrl={logoUrl}
          countryCode={countryCode}
          verified={verified}
          reviews={reviews}
        />
      )}
      <div className={styles.container + " " + className}>
        <img alt={`${name} logo`} src={logoUrl} className={styles.logo} />
        <div className={styles.info}>
          <p className={styles.name}>{name}</p>
          <p className={styles.website}>{website ? website : "No website"} </p>
        </div>
        <p className={styles.country}>{countryCodesObj[countryCode]}</p>
        <p className={styles.verified}>
          {verified ? "Verified" : "Not verified"}
        </p>
        <SpecialButton
          className={styles.editButton}
          onClick={() => setEditCompany()}
        >
          Edit
        </SpecialButton>
        {reviewsButton}
        <SpecialButton
          className={styles.deleteButton}
          onDoubleClick={deleteCompany}
        >
          Dbl click to delete
        </SpecialButton>
        {reviews.length > 0 && (
          <div className={styles.reviewsContainer}>
            {reviews.map((review, i_review) => {
              return (
                <Review
                  className={styles.review}
                  text={review.text}
                  title={review.title}
                  images={review.images}
                  key={review.id}
                  user={review.user}
                  userEmail={review.user.email}
                  userName={review.user.name}
                  userPhone={review.user.phone}
                  reviewId={review.id}
                  setDeleted={() => {
                    setReviews(reviews.filter((item, i) => i !== i_review));
                  }}
                />
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default CompanyPage;
