import React from "react";
import { Formik } from "formik";
import CountryCodes from "country-codes-list";
import Select from "react-select";
import styles from "./CreateCompanyForm.module.css";
import PrimaryButton from "../GPButton/PrimaryButton";
import { createCompany, updateCompany } from "../../utils/APICalls";
import Dropzone from "../Dropzone/Dropzone";

const CreateCompanyForm = ({ name = "", editCompany = null }) => {
  const countryCodesObj = CountryCodes.customList(
    "countryCode",
    "{countryNameEn}"
  );
  const countryOptions = Object.keys(countryCodesObj).map(countryCode => ({
    label: countryCodesObj[countryCode],
    value: countryCode
  }));
  const defaultSelectOption = { label: "Portugal", value: "PT" };

  const onCompanyCreate = (values, formikActions) => {
    createCompany({
      name: values.companyName,
      countryCode: values.countryName.value,
      verified: false,
      logo: values.logoImage,
      website: values.website
    })
      .then(response => {
        if (response._ok) {
          formikActions.resetForm();
          formikActions.setStatus({ success: true });
        } else {
          formikActions.setErrors(response.error);
          formikActions.setStatus({ success: false });
        }
        formikActions.setSubmitting(false);
      })
      .catch(error => {
        formikActions.setErrors(error);
        formikActions.setSubmitting(false);
        formikActions.setStatus({ success: false });
      });
  };

  const onCompanyUpdate = (values, formikActions) => {
    updateCompany({
      companyId: editCompany.id,
      name: values.companyName,
      countryCode: values.countryName.value,
      verified: false,
      logo: values.logoImage,
      website: values.website
    })
      .then(response => {
        if (response._ok) {
          formikActions.resetForm();
          formikActions.setStatus({ success: true });
          formikActions.setFieldValue(name, "");
        } else {
          formikActions.setErrors(response.error);
          formikActions.setStatus({ success: false });
        }
      })
      .catch(error => {
        formikActions.setSubmitting(false);
        formikActions.setStatus({ success: false });
      });
  };

  return (
    <Formik
      initialValues={{
        companyName: editCompany ? editCompany.name : name,
        countryName: editCompany
          ? {
              label: editCompany.countryCode,
              value: countryCodesObj[editCompany.countryCode]
            }
          : defaultSelectOption,
        logoImage: null,
        verified: editCompany ? editCompany.verified : "False",
        website: editCompany && editCompany.website ? editCompany.website : ""
      }}
      initialStatus={{ success: false }}
      onSubmit={editCompany ? onCompanyUpdate : onCompanyCreate}
    >
      {({
        handleChange,
        values,
        setFieldValue,
        handleSubmit,
        isSubmitting,
        status,
        submitCount,
        errors
      }) => {
        return (
          <form className={styles.formContainer} onSubmit={handleSubmit}>
            <div>
              <p>Name</p>
              <input
                className={styles.nameInput}
                name="companyName"
                value={values.companyName}
                onChange={handleChange}
              />
            </div>
            <div>
              <p>Website</p>
              <input
                className={styles.nameInput}
                name="website"
                value={values.website}
                onChange={handleChange}
              />
            </div>
            <div>
              <label>Country</label>
              <Select
                defaultValue={
                  editCompany
                    ? {
                        value: editCompany.countryCode,
                        label: countryCodesObj[editCompany.countryCode]
                      }
                    : defaultSelectOption
                }
                options={countryOptions}
                onChange={option => {
                  setFieldValue("countryName", option);
                }}
                name="countryName"
              />
            </div>
            <div>
              <label>Logo</label>
              <Dropzone
                onDrop={acceptedFiles => {
                  setFieldValue("logoImage", acceptedFiles[0]);
                }}
                images={values.logoImage ? [values.logoImage] : []}
              />
            </div>
            <PrimaryButton type="submit" className={styles.submitButton}>
              {editCompany ? "Update Company" : "Create Company"}
            </PrimaryButton>
            {status.success && !isSubmitting && (
              <p>Successfully {editCompany ? "updated" : "created"}</p>
            )}
            {!status.success &&
              !isSubmitting &&
              submitCount > 0 &&
              ((!editCompany && (
                <p>
                  Couldn't create company. Probably the company exists already.
                </p>
              )) || <p>Couldn't update company.</p>)}
            {errors && <p>{JSON.stringify(errors)}</p>}
          </form>
        );
      }}
    </Formik>
  );
};

export default CreateCompanyForm;
