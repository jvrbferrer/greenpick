import React from "react";
import { Carousel } from "react-bootstrap";
import styles from "./Review.module.css";
import PrimaryButton from "../GPButton/PrimaryButton";
import { deleteReview } from "../../utils/APICalls";
import { useHistory } from "react-router-dom";

const Review = ({
  images,
  title,
  text,
  userEmail,
  userName,
  userPhone,
  className,
  reviewId,
  setDeleted,
  ...props
}) => {
  const history = useHistory();

  return (
    <div className={styles.container + " " + className} {...props}>
      <Carousel className={styles.carousel} indicators={false} interval={null}>
        {images.map(image_url => {
          return (
            <Carousel.Item key={image_url}>
              <div className={styles.imgContainer}>
                <img alt="" src={image_url} className={styles.carouselImg} />
              </div>
            </Carousel.Item>
          );
        })}
      </Carousel>
      <h4 className={styles.title}>{title}</h4>
      <p className={styles.text}>{text}</p>
      <p className={styles.text}>
        By{" "}
        {userName
          ? userName
          : userEmail
          ? userEmail
          : userPhone
          ? userPhone
          : "anonymous"}
      </p>
      <PrimaryButton
        onClick={() => {
          history.push("/admin/reviews", {
            addComment: {
              reviewId
            }
          });
        }}
        className={styles.addCommentButton}
      >
        Add comment
      </PrimaryButton>
      <PrimaryButton
        onDoubleClick={() => {
          deleteReview({ reviewId }).then(res => {
            if (res._ok) {
              setDeleted();
            }
          });
        }}
        className={styles.deleteButton}
      >
        Delete (dbl click)
      </PrimaryButton>
    </div>
  );
};

export default Review;
