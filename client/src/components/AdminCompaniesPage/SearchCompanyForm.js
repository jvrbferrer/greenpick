import React from "react";
import { Formik } from "formik";
import AsyncCreatableSelect from "react-select/async-creatable";
import PrimaryButton from "../GPButton/PrimaryButton";
import styles from "./SearchCompanyForm.module.css";
import { searchCompany, findCompany } from "../../utils/APICalls";

const SearchCompanyForm = ({
  setSelectedCompany,
  setCreateCompanyName,
  className
}) => {
  const getCompanyOptions = async beginningStr => {
    const { companies } = await searchCompany({ begName: beginningStr });
    return companies.map(({ id, name }) => ({ label: name, value: id }));
  };

  const onCompanySearch = ({ company }, formikActions) => {
    findCompany({ companyId: company.value })
      .then(response => {
        setSelectedCompany(response);
        formikActions.setSubmitting(false);
      })
      .catch(error => {
        console.log(error);
        formikActions.setSubmitting(false);
      });
  };

  return (
    <Formik initialValues={{ company: {} }} onSubmit={onCompanySearch}>
      {({ values, setFieldValue, handleSubmit }) => {
        return (
          <form
            className={styles.formContainer + " " + className}
            onSubmit={handleSubmit}
          >
            <AsyncCreatableSelect
              name="companyName"
              isClearable
              loadOptions={getCompanyOptions}
              getOptionLabel={option => option.label}
              getOptionValue={option => option.value}
              onCreateOption={name => setCreateCompanyName(name)}
              onChange={(option, action) => {
                setFieldValue("company", option);
              }}
            />
            <div>
              <PrimaryButton type="submit">Get company</PrimaryButton>
            </div>
          </form>
        );
      }}
    </Formik>
  );
};

export default SearchCompanyForm;
