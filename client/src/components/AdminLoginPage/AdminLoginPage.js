import React from "react";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { Formik } from "formik";
import Form from "react-bootstrap/Form";
import PrimaryButton from "../GPButton/PrimaryButton";
import { authenticate } from "../../redux/authentication";
import styles from "./AdminLoginPage.module.css";

const schema = Yup.object({
  email: Yup.string().email("Invalid email"),
  password: Yup.string(),
});

const AdminLoginPage = () => {
  const dispatch = useDispatch();

  const onLoginSubmit = ({ email, password }, formikActions) => {
    dispatch(authenticate({ email, password })).catch(
      formikActions.resetForm()
    );
  };

  return (
    <div className={styles.container}>
      <Formik
        validationSchema={schema}
        initialValues={{ email: "", password: "" }}
        onSubmit={onLoginSubmit}
      >
        {({ handleSubmit, handleChange, values, errors }) => {
          return (
            <Form
              noValidate
              onSubmit={handleSubmit}
              className={styles.loginForm}
            >
              <div className={styles.formGroup + " " + styles.formSeparator}>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Email"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  isInvalid={!!errors.email}
                />
                {!!errors.email && <p>{errors.email}</p>}
              </div>
              <div className={styles.formGroup + " " + styles.formSeparator}>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  isInvalid={!!errors.password}
                />
                {!!errors.password && <p>{errors.password}</p>}
              </div>
              <PrimaryButton className={styles.loginButton} type="submit">
                Login
              </PrimaryButton>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default AdminLoginPage;
