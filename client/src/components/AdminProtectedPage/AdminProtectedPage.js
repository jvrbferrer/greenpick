import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import AdminLoginPage from "../AdminLoginPage/AdminLoginPage";
import styles from "./AdminProtectedPage.module.css";
import { logout } from "../../redux/authentication";
import PrimaryButton from "../GPButton/PrimaryButton";

const AdminProtectedPage = ({ children }) => {
  const dispatch = useDispatch();
  const authentication = useSelector(state => {
    return state.authentication.authenticated;
  });
  if (!authentication.admin) {
    return <AdminLoginPage />;
  }
  return (
    <>
      <div className={styles.navigation}>
        <NavLink
          to="/admin"
          className={styles.navigationLink}
          activeClassName={styles.activeNavigationLink}
          exact
        >
          Admin Home
        </NavLink>
        <NavLink
          to="/admin/companies"
          className={styles.navigationLink}
          activeClassName={styles.activeNavigationLink}
        >
          Companies
        </NavLink>
        <NavLink
          to="/admin/reviews"
          className={styles.navigationLink}
          activeClassName={styles.activeNavigationLink}
        >
          Reviews
        </NavLink>
        <NavLink
          to="/admin/users"
          className={styles.navigationLink}
          activeClassName={styles.activeNavigationLink}
        >
          Users
        </NavLink>
        <NavLink
          to="/admin/codes"
          className={styles.navigationLink}
          activeClassName={styles.activeNavigationLink}
        >
          Codes
        </NavLink>
        <PrimaryButton
          className={styles.logoutButton}
          onClick={() => dispatch(logout())}
        >
          Logout
        </PrimaryButton>
      </div>
      {children}
    </>
  );
};

export default AdminProtectedPage;
