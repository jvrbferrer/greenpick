import React from "react";
import styles from "./AdminReviewsPage.module.css";
import CreateReviewForm from "./CreateReviewForm";
import CreateCommentForm from "./CreateCommentForm";
import { useLocation } from "react-router-dom";

const AdminReviewsPage = () => {
  const location = useLocation();
  const userId = location.state ? location.state.id : null;
  const addComment = location.state ? location.state.addComment : null;
  return (
    <div className={styles.container}>
      <div>
        <h2>Create review</h2>
        <CreateReviewForm userId={userId} />
      </div>
      <div>
        <h2>Add Comment</h2>
        <CreateCommentForm
          reviewId={addComment ? addComment.reviewId : null}
          userId={addComment ? addComment.userId : null}
        />
      </div>
    </div>
  );
};

export default AdminReviewsPage;
