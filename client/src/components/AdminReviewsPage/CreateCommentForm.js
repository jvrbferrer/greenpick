import React from "react";
import { Formik } from "formik";
import { createComment } from "../../utils/APICalls";
import PrimaryButton from "../GPButton/PrimaryButton";
import styles from "./CreateCommentForm.module.css";

const CreateCommentForm = ({ userId, reviewId }) => {
  const onCreate = (values, formikActions) => {
    createComment({
      userId: values.userId,
      reviewId: values.reviewId,
      text: values.text,
      createdAt: values.timeCreated
    })
      .then(response => {
        if (response._ok) {
          formikActions.setFieldValue("text", "");
          formikActions.setFieldValue("timeCreated", null);
          formikActions.setStatus({ success: true });
        } else {
          formikActions.setErrors(response.error);
          formikActions.setStatus({ success: false });
        }
        formikActions.setSubmitting(false);
      })
      .catch(error => {
        console.log(error);
        formikActions.setSubmitting(false);
        formikActions.setStatus({ success: false });
      });
  };
  return (
    <Formik
      initialValues={{
        reviewId: reviewId || "",
        userId: userId || "",
        text: "",
        timeCreated: null
      }}
      onSubmit={onCreate}
      initialStatus={{ success: false }}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        submitCount,
        isSubmitting,
        status,
        errors
      }) => {
        return (
          <form
            noValidate
            onSubmit={handleSubmit}
            className={styles.CreateCommentForm}
          >
            <div className={styles.formGroup}>
              <p>User id</p>
              <input
                type="text"
                value={values.userId}
                name="userId"
                onChange={handleChange}
              />
            </div>
            <div className={styles.formGroup}>
              <p>Text</p>
              <textarea
                placeholder="Comment..."
                onChange={handleChange}
                cols="3"
                name="text"
                value={values.text}
              />
            </div>
            <div className={styles.formGroup}>
              <p>Review id</p>
              <input
                type="text"
                value={values.reviewId}
                name="reviewId"
                onChange={handleChange}
              />
            </div>
            <div className={styles.formGroup}>
              <p>Created at</p>
              <input
                type="datetime-local"
                value={values.timeCreated}
                name="timeCreated"
                onChange={handleChange}
              />
            </div>
            <PrimaryButton type="submit">Add comment</PrimaryButton>
            {status.success && !isSubmitting && <p>Successfully created</p>}
            {!status.success && !isSubmitting && submitCount > 0 && (
              <p>{JSON.stringify(errors)}</p>
            )}
          </form>
        );
      }}
    </Formik>
  );
};

export default CreateCommentForm;
