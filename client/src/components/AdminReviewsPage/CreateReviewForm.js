import React from "react";
import { Form } from "react-bootstrap";
import { Formik } from "formik";
import Dropzone from "../Dropzone/Dropzone";
import PrimaryButton from "../GPButton/PrimaryButton";
import styles from "./CreateReviewForm.module.css";
import { createReview } from "../../utils/APICalls";
import AsyncSelect from "react-select/async";
import { searchCompany } from "../../utils/APICalls";

const CreateReviewForm = ({ userId }) => {
  const getCompanyOptions = async beginningStr => {
    const { companies } = await searchCompany({ begName: beginningStr });
    return companies.map(({ id, name }) => ({ label: name, value: id }));
  };
  const onReviewSubmit = (values, formikActions) => {
    if (values.images.length <= 3) {
      createReview({
        userId: values.userId,
        companyId: values.company.value,
        title: values.title,
        text: values.description,
        images: values.images,
        createdAt: values.timeCreated
      })
        .then(response => {
          if (response._ok) {
            formikActions.setFieldValue("images", []);
            formikActions.setFieldValue("description", "");
            formikActions.setFieldValue("title", "");
            formikActions.setStatus({ success: true });
          } else {
            formikActions.setErrors(response.error);
            formikActions.setStatus({ success: false });
          }
          formikActions.setSubmitting(false);
        })
        .catch(error => {
          console.log(error);
          formikActions.setSubmitting(false);
          formikActions.setStatus({ success: false });
        });
    }
  };
  return (
    <Formik
      initialValues={{
        images: [],
        company: null,
        title: "",
        description: "",
        userId: userId ? userId : "",
        timeCreated: ""
      }}
      onSubmit={onReviewSubmit}
      initialStatus={{ success: false }}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        setFieldValue,
        isSubmitting,
        status,
        submitCount,
        errors
      }) => {
        return (
          <Form
            noValidate
            onSubmit={handleSubmit}
            className={styles.createReviewForm}
          >
            <div className={styles.formGroup}>
              <Form.Label>User Id</Form.Label>
              <input
                type="text"
                value={values.userId}
                onChange={handleChange}
                name="userId"
                className={styles.formInput}
              />
            </div>
            <div className={styles.formGroup}>
              <Form.Label>Company name</Form.Label>
              <AsyncSelect
                name="companyName"
                isClearable
                loadOptions={getCompanyOptions}
                getOptionLabel={option => option.label}
                getOptionValue={option => option.value}
                onChange={(option, action) => {
                  setFieldValue("company", option);
                }}
              />
            </div>
            <div className={styles.formGroup}>
              <Form.Label>Created at</Form.Label>
              <input
                type="datetime-local"
                value={values.timeCreated}
                name="timeCreated"
                onChange={handleChange}
              />
            </div>
            <div className={styles.formGroup}>
              <Form.Label>Review Title</Form.Label>
              <input
                className={styles.formInput}
                placeholder="Title"
                onChange={handleChange}
                name="title"
                value={values.title}
              />
            </div>
            <div className={styles.formGroup}>
              <Form.Label>Review Text</Form.Label>
              <textarea
                className={styles.formInput + " " + styles.textarea}
                placeholder="Review..."
                onChange={handleChange}
                cols="10"
                name="description"
                value={values.description}
              />
            </div>
            <Form.Group>
              <Form.Label>Review Images</Form.Label>
              <Dropzone
                onDrop={acceptedFiles => {
                  const newImages = [...acceptedFiles, ...values.images].slice(
                    0,
                    3
                  );
                  setFieldValue("images", newImages);
                }}
                images={values.images}
              />
            </Form.Group>
            <PrimaryButton type="submit">Create Review</PrimaryButton>
            {status.success && !isSubmitting && <p>Successfully created</p>}
            {!status.success && !isSubmitting && submitCount > 0 && (
              <p>{JSON.stringify(errors)}</p>
            )}
          </Form>
        );
      }}
    </Formik>
  );
};

export default CreateReviewForm;
