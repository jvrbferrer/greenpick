import React, { useState } from "react";
import styles from "./AdminUsersPage.module.css";
import SearchUserForm from "./SearchUserForm";
import CreateUserForm from "./CreateUserForm";
import UserCard from "./UserCard";

const AdminUsersPage = () => {
  const [foundUsers, setFoundUsers] = useState(null);
  const [selectedUser, setSelectedUser] = useState(null);
  const [editUser, setEditUser] = useState(null);

  let mainContent = null;
  if (selectedUser) {
    mainContent = (
      <div className={styles.searchResultContainer}>
        <UserCard
          name={selectedUser.name}
          email={selectedUser.email}
          phone={selectedUser.phone}
          signedUp={selectedUser.signed_up}
          id={selectedUser.id}
          setEditUser={setEditUser}
        />
      </div>
    );
  } else if (foundUsers && foundUsers.length === 0) {
    mainContent = (
      <div className={styles.searchResultContainer}>
        <h2 className={styles.noUsersFoundTitle}>No users found</h2>
      </div>
    );
  } else if (foundUsers && foundUsers.length > 0) {
    mainContent = (
      <div className={styles.searchResultContainer}>
        <h2>Users Found</h2>
        {foundUsers.map((user) => {
          return (
            <UserCard
              className={styles.userCard}
              name={user.name}
              email={user.email}
              phone={user.phone}
              signedUp={user.signed_up}
              id={user.id}
              key={user.id}
              setEditUser={setEditUser}
            />
          );
        })}
      </div>
    );
  }
  return (
    <div className={styles.container}>
      <div className={styles.searchForm}>
        <h2>Search User</h2>
        <SearchUserForm setFoundUsers={setFoundUsers} />
      </div>
      <div className={styles.createForm}>
        <h2>Create User</h2>
        <CreateUserForm
          edit={editUser}
          finishEditting={() => {
            setEditUser(null);
          }}
          setCreatedUser={setSelectedUser}
        />
      </div>
      {mainContent}
    </div>
  );
};

export default AdminUsersPage;
