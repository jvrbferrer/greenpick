import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import styles from "./CreateUserForm.module.css";
import PrimaryButton from "../GPButton/PrimaryButton";
import { createUser, updateUser } from "../../utils/APICalls";

const schema = Yup.object({
  email: Yup.string().email("Invalid email"),
  name: Yup.string().matches(/[a-zA-Z ]+/, "Invalid name"),
  phone: Yup.string().matches(/\+?[1-9]+/, "Invalid phone number"),
  password: Yup.string(),
});

const CreateUserForm = ({ setCreatedUser, finishEditting, edit }) => {
  const handleCreateSubmit = (values, formikActions) => {
    createUser({ ...values, sendEmail: false, signedUp: false })
      .then((response) => {
        formikActions.setSubmitting(false);
        if (!response._ok) {
          formikActions.setErrors(response.error);
          formikActions.setStatus({ success: false });
          console.log(response.error);
          return;
        }
        setCreatedUser(response.user);
        formikActions.resetForm();
        formikActions.setStatus({ success: true });
      })
      .catch((error) => {
        console.log(error);
        formikActions.setSubmitting(false);
        formikActions.setStatus({ success: false });
      });
  };

  const handleUpdateSubmit = (values, formikActions) => {
    updateUser({ ...values, signedUp: false, id: edit.id })
      .then((response) => {
        formikActions.setSubmitting(false);
        if (!response._ok) {
          formikActions.setErrors(response.error);
          formikActions.setStatus({ success: false });
          console.log(response.error);
          return;
        }
        setCreatedUser(response.user);
        finishEditting();
        formikActions.resetForm();
        formikActions.setStatus({ success: true });
      })
      .catch((error) => {
        console.log(error);
        formikActions.setSubmitting(false);
        formikActions.setStatus({ success: false });
      });
  };
  return (
    <Formik
      validationSchema={schema}
      initialValues={{
        email: edit ? edit.email : "",
        name: edit ? edit.name : "",
        phone: edit ? edit.phone : "",
        password: "",
      }}
      onSubmit={edit ? handleUpdateSubmit : handleCreateSubmit}
      initialStatus={{ success: false }}
    >
      {({ handleSubmit, handleChange, values, errors, status }) => {
        return (
          <form onSubmit={handleSubmit}>
            <div className={styles.inputGroup}>
              <label>Name</label>
              <input
                type="text"
                onChange={handleChange}
                value={values.name}
                name="name"
              ></input>
              <p>{errors.name}</p>
            </div>
            <div className={styles.inputGroup}>
              <label>Email</label>
              <input
                type="email"
                onChange={handleChange}
                value={values.email}
                name="email"
              ></input>
              <p>{errors.name}</p>
            </div>
            <div className={styles.inputGroup}>
              <label>Phone number</label>
              <input
                type="text"
                onChange={handleChange}
                value={values.phone}
                name="phone"
              ></input>
              <p>{errors.phone}</p>
            </div>
            <div className={styles.inputGroup}>
              <label>Password. Anything.</label>
              <input
                type="text"
                onChange={handleChange}
                value={values.password}
                name="password"
              ></input>
              <p>{errors.password}</p>
            </div>
            <PrimaryButton type="submit" className={styles.submitButton}>
              {edit ? "Update user" : "Create user"}
            </PrimaryButton>

            {status.success && <p>Success!</p>}
          </form>
        );
      }}
    </Formik>
  );
};

export default CreateUserForm;
