import React from "react";
import { Formik } from "formik";
import { findUsersByAllFields } from "../../utils/APICalls";
import PrimaryButton from "../GPButton/PrimaryButton";
import styles from "./SearchUserForm.module.css";

const SearchUserForm = ({ setFoundUsers }) => {
  const handleSubmit = (values, formikActions) => {
    findUsersByAllFields({ query: values.query })
      .then(response => {
        if (!response._ok) {
          formikActions.setErrors(response.error);
          formikActions.setSubmitting(false);
          return;
        }
        setFoundUsers(response.users);
        formikActions.setSubmitting(false);
        formikActions.resetForm();
        formikActions.setStatus({ success: true });
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <>
      <Formik novalidate initialValues={{ query: "" }} onSubmit={handleSubmit}>
        {({ handleSubmit, handleChange, values }) => {
          return (
            <form onSubmit={handleSubmit} className={styles.form}>
              <input
                type="text"
                value={values.query}
                onChange={handleChange}
                name="query"
                placeholder="Phone number, email, name"
              />
              <PrimaryButton type="submit">Search</PrimaryButton>
            </form>
          );
        }}
      </Formik>
    </>
  );
};

export default SearchUserForm;
