import React from "react";
import styles from "./UserCard.module.css";
import SpecialButton from "../GPButton/SpecialButton";
import { useHistory } from "react-router-dom";

const UserCard = ({
  name,
  id,
  email,
  phone,
  signedUp,
  className,
  setEditUser,
}) => {
  const history = useHistory();

  const addReview = () => {
    history.push("/admin/reviews", { id });
  };

  const editUser = () => {
    setEditUser({
      id,
      name: name ? name : "",
      email: email ? email : "",
      phone: phone ? phone : "",
    });
  };

  return (
    <div className={styles.container + " " + className}>
      <p className={styles.name}>{name}</p>
      <p className={styles.email}>{email}</p>
      <p className={styles.phone}>{phone}</p>
      <p className={styles.id}>ID: {id}</p>
      <p className={styles.signedUp}>Signed up:{signedUp}</p>
      <SpecialButton className={styles.addReview} onClick={addReview}>
        Add Review
      </SpecialButton>
      <SpecialButton className={styles.editUser} onClick={editUser}>
        Edit
      </SpecialButton>
    </div>
  );
};

export default UserCard;
