import React from "react";
import styles from "./CollectRewardPage.module.css";
import { useChangeNavbarColor } from "../../utils/Hooks";
import CodeSubmitForm from "../Forms/CodeSubmitForm";
import SocialNetworks from "../SocialNetworks/SocialNetworks";
import ForestBar from "../ForestBar/ForestBar";

const CollectRewardPage = ({ setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#ffffff"));
  return (
    <>
      <div className={styles.container}>
        <div className={styles.formContainer}>
          <h1 className={styles.title}>Collect your eco reward</h1>
          <CodeSubmitForm className={styles.codeSubmitForm} />
          <p className={styles.formSubscribe}>
            New to greenpick?{" "}
            <a href="#subscribeForm" className={styles.subscribeLink}>
              Subscribe
            </a>{" "}
            to our latest insights
          </p>
        </div>
        <div className={styles.infoContainer}>
          <h2 className={styles.subTitle}>How do I get an eco reward?</h2>
          <p className={styles.text}>
            PROBABLY NEEDS A CHANGE Join our facebook group or subreddit and
            make eco reviews about products or brands you've used. Onced you
            reach 10, we'll send you a code to collect your reward here.
          </p>
          <h2 className={styles.subTitle}>What are eco-rewards?</h2>
          <p className={styles.text}>
            At the moment the eco reward we give during our early tests is a
            tree planted in your behalf and certified by us. In the future we'll
            have all different initiatives to offset your carbon footprint as a
            reward for your amazing contribution.
          </p>
          <p className={styles.joinUsLabel}>Follow us</p>
          <SocialNetworks />
        </div>
      </div>
      <ForestBar />
    </>
  );
};

export default CollectRewardPage;
