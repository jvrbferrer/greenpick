import React from "react";
import { Form, FormGroup, Col } from "react-bootstrap";
import * as Yup from "yup";
import { Formik } from "formik";
import styles from "./ContactForm.module.css";
import PrimaryButton from "../GPButton/PrimaryButton";
import { receiveContact } from "../../utils/APICalls";

const schema = Yup.object({
  name: Yup.string().required("Required"),
  email: Yup.string()
    .required("Required")
    .email("Invalid email"),
  text: Yup.string()
    .required("Required")
    .max(2500, "Max number of characters is 2500")
});

const ContactForm = props => {
  const onTextSubmit = ({ name, email, text }, formikActions) => {
    receiveContact({
      info: {
        name,
        email,
        text
      }
    })
      .then(response => {
        if (!response._ok) {
          formikActions.setErrors(response.error);
        }
        formikActions.setSubmitting(false);
        formikActions.setStatus({ success: !!response._ok });
      })
      .catch(error => {
        formikActions.setSubmitting(false);
      });
  };

  return (
    <div className={styles.formContainer + " " + props.className}>
      <Formik
        validationSchema={schema}
        initialValues={{ name: "", email: "", text: "" }}
        onSubmit={onTextSubmit}
        initialStatus={{ success: false }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          status,
          errors,
          touched,
          isSubmitting
        }) => {
          return (
            <Form noValidate onSubmit={handleSubmit}>
              <FormGroup>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  placeholder="Full name"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.name && errors.name}
                  spellCheck="false"
                />
                <Form.Control.Feedback type="invalid">
                  {errors.name}
                </Form.Control.Feedback>
              </FormGroup>
              <FormGroup>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  placeholder="youremail@gmail.com"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.email && errors.email}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </FormGroup>
              <FormGroup>
                <Form.Label>Message</Form.Label>
                <Form.Control
                  name="text"
                  as="textarea"
                  rows="5"
                  placeholder="Have a question for us?"
                  value={values.text}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isInvalid={touched.text && errors.text}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.text}
                </Form.Control.Feedback>
              </FormGroup>
              <Form.Row>
                <Col
                  xs={{ span: 12, order: 2 }}
                  sm={{ span: 6, order: 1 }}
                  className="mt-sm-0 mt-4"
                >
                  {status.success && (
                    <div>
                      <p>We got your message and will get back to you asap</p>
                    </div>
                  )}
                </Col>
                <Col xs={{ span: 12, order: 1 }} sm={{ span: 6, order: 2 }}>
                  <PrimaryButton
                    className={styles.formSubmitButton}
                    type="submit"
                    disabled={isSubmitting}
                  >
                    Send message
                  </PrimaryButton>
                </Col>
              </Form.Row>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default ContactForm;
