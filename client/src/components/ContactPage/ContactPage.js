import React from "react";
import styles from "./ContactPage.module.css";
import ContactForm from "./ContactForm";
import ForestBar from "../ForestBar/ForestBar";
import SocialNetworks from "../SocialNetworks/SocialNetworks";
import { useChangeNavbarColor } from "../../utils/Hooks";

const ContactPage = ({ setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#FFFFFF"));

  return (
    <>
      <h1 className={styles.title}>Contact us</h1>
      <div className={styles.container}>
        <div className={styles.formContainer}>
          <ContactForm className={styles.contactUsForm} />
        </div>
        <div className={styles.infoContainer}>
          <div className="d-flex flex-column mt-md-0 mt-5">
            <p className={styles.infoLabel}>Email</p>
            <p className={styles.info}>inbox@greenpick.co</p>
            <p className={styles.infoLabel}>Join us</p>
            <SocialNetworks />
          </div>
        </div>
      </div>
      <ForestBar backgroundColor="#ffffff" />
    </>
  );
};

export default ContactPage;
