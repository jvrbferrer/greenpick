import React from "react";
import { useDropzone } from "react-dropzone";
import styles from "./Dropzone.module.css";

function Dropzone({ onDrop, images }) {
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: "image/*"
  });
  return (
    <div {...getRootProps({ className: styles.dropzoneContainer })}>
      <input {...getInputProps()} />
      {isDragActive ? (
        <p key="hey">Drop the files here ...</p>
      ) : (
        images.map((image, i) => {
          return (
            <p key={image.name} key_right={image.name}>
              {image.path}
            </p>
          );
        })
      )}
    </div>
  );
}

export default Dropzone;
