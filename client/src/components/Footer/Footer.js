import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";
import styles from "./Footer.module.css";
import logoNameWhite from "../../assets/img/group-white.svg";
import SubscribeForm from "../Forms/SubscribeForm";
import SocialNetworks from "../SocialNetworks/SocialNetworks";
import { Link } from "react-router-dom";
import FooterLink from "./FooterLink";

const Footer = (props) => {
  return (
    <Container fluid className={"pt-4 " + styles.footer}>
      <Row className="justify-content-md-between">
        <Col xl="auto" xs={12}>
          <div className={"ml-xl-5 ml-3 mr-5 " + styles.logoContainer}>
            <Link to="/">
              <img src={logoNameWhite} alt="Greenpick logo" className={""} />
            </Link>
          </div>
        </Col>
        <Col md="auto" xs={12}>
          <Nav className={"flex-column mt-4 mt-xl-0 mr-4 "}>
            <p className={"px-3 " + styles.pagesText + " pt-0"}>Pages</p>
            <FooterLink to="/" exact>
              Home
            </FooterLink>
            <FooterLink to="/about/" strict>
              About
            </FooterLink>
            <FooterLink to="/contact/" exact>
              Contact
            </FooterLink>
            <FooterLink to="/terms&conditions/" exact>
              Terms & Conditions
            </FooterLink>
          </Nav>
        </Col>
        <Col md="auto" xs={12}>
          <div className="flex-column mr-md-5 mt-4 mt-xl-0">
            <p className={"ml-3 " + styles.pagesText}>Socials</p>
            <SocialNetworks className={"ml-3 " + styles.socialNetworks} />
          </div>
        </Col>
        <Col md xs={12}>
          <div className="d-flex flex-column align-items-md-end mr-2 mr-lg-5 mt-4 mt-xl-0 ml-3 ml-md-0">
            <p className={styles.callForSubscription}>
              Notify me when the platform launches
            </p>
            <SubscribeForm className={styles.formContainer} />
            <p className={"mt-5 pt-3 " + styles.copyRightsText}>
              @2020 Greenpick. All rights reserved
            </p>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Footer;
