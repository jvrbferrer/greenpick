import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./Footer.module.css";

const FooterLink = props => {
  return (
    <NavLink
      {...props}
      className={styles.pageLink}
      activeClassName={styles.activePageLink}
    />
  );
};

export default FooterLink;
