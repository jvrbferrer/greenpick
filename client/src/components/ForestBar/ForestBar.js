import React from "react";
import mobileForest from "../../assets/img/mobile-forest.svg";
import ipadForest from "../../assets/img/ipad-forest.svg";
import desktopForest from "../../assets/img/desktop-forest.svg";
import styles from "./ForestBar.module.css";

const ForestBar = ({ backgroundColor }) => {
  return (
    <div
      className={
        "d-flex flex-column-reverse align-items-center " + styles.mainContainer
      }
    >
      <picture>
        <source media="(min-width: 650px)" srcSet={desktopForest} />
        <source media="(min-width: 351px)" srcSet={ipadForest} />
        <img
          src={mobileForest}
          alt="Cute forest"
          className={styles.forest}
          style={{ backgroundColor: backgroundColor }}
        />
      </picture>
    </div>
  );
};

ForestBar.defaultProps = {
  backgroundColor: "white"
};

export default ForestBar;
