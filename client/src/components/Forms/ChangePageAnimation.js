import React, { useState, useEffect } from "react";
import styles from "./ChangePageAnimation.module.css";
import lightGreenLogo from "../../assets/img/LighGreenLogo.svg";

const ChangePageAnimation = ({ timeMs, nSteps }) => {
  const [submitStep, setSubmitStep] = useState(1);
  useEffect(() => {
    let to1 = setTimeout(() => {
      setSubmitStep(2);
    }, timeMs / 2 / (nSteps + 1));
    let to2 = setTimeout(() => {
      setSubmitStep(3);
    }, (timeMs * 2) / (nSteps + 1));
    return () => {
      clearTimeout(to1);
      clearTimeout(to2);
    };
  }, [timeMs, nSteps]);
  return (
    <div className={styles.container}>
      <img src={lightGreenLogo} alt="Greenpick logo" className={styles.img} />
      <p className={styles.text}>Preparing your reward</p>
      <p className={styles.info}>
        {submitStep} out of {nSteps} completed
      </p>
    </div>
  );
};

export default ChangePageAnimation;
