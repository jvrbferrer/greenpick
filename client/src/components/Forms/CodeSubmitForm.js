import React, { useState } from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "./FormGroup";
import PrimaryButton from "../GPButton/PrimaryButton";
import { Redirect } from "react-router-dom";
import { submitCode } from "../../utils/APICalls";
import styles from "./CodeSubmitForm.module.css";
import * as Yup from "yup";
import { Formik } from "formik";
import ChangePageAnimation from "./ChangePageAnimation";

const schema = Yup.object({
  name: Yup.string(),
  email: Yup.string()
    .required("Required")
    .email("Invalid email"),
  codeString: Yup.string().required("Required")
});

const CodeSubmitForm = props => {
  const [receivedPrize, setReceivedPrize] = useState(null);

  const onPrizeCodeSubmit = (values, formikActions) => {
    submitCode(values)
      .then(response => {
        if (!response._ok) {
          formikActions.setErrors(response.error);
          formikActions.setSubmitting(false);
          formikActions.setStatus({ success: false });
          return;
        }
        setReceivedPrize(response.prize);
        setTimeout(() => {
          formikActions.setSubmitting(false);
          formikActions.setStatus({ success: true });
        }, 3000);
      })
      .catch(error => {
        formikActions.setSubmitting(false);
      });
  };

  return (
    <div className={styles.formContainer + " " + props.className}>
      <Formik
        validationSchema={schema}
        initialValues={{ name: "", email: "", codeString: "" }}
        onSubmit={onPrizeCodeSubmit}
        initialStatus={{ success: false }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          isSubmitting,
          status,
          errors,
          touched
        }) => {
          return (
            <>
              {isSubmitting && <ChangePageAnimation timeMs={3000} nSteps={3} />}
              {status.success && (
                <Redirect
                  push
                  to={{
                    pathname: "/reward",
                    state: { ...values, prize: receivedPrize }
                  }}
                />
              )}
              <Form noValidate onSubmit={handleSubmit}>
                <FormGroup>
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    placeholder="Name"
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.name && errors.name}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.name}
                  </Form.Control.Feedback>
                </FormGroup>
                <FormGroup>
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    name="email"
                    placeholder="youremail@gmail.com"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.email && errors.email}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.email}
                  </Form.Control.Feedback>
                </FormGroup>
                <FormGroup>
                  <Form.Label>Code</Form.Label>
                  <Form.Control
                    className="input"
                    type="text"
                    name="codeString"
                    placeholder="Got a code? Place it here"
                    autoComplete="off"
                    spellCheck="false"
                    value={values.codeString}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    isInvalid={touched.codeString && errors.codeString}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.codeString}
                  </Form.Control.Feedback>
                </FormGroup>
                <Form.Row>
                  <Col sm={{ span: 6, offset: 6 }}>
                    <PrimaryButton
                      className={styles.formSubmitButton}
                      type="submit"
                      disabled={isSubmitting}
                    >
                      Collect your reward
                    </PrimaryButton>
                  </Col>
                </Form.Row>
              </Form>
            </>
          );
        }}
      </Formik>
    </div>
  );
};

export default CodeSubmitForm;
