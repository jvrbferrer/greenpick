import React from "react";
import styles from "./CodeSubmitForm.module.css";
import Form from "react-bootstrap/Form";

const FormGroup = (props) => {
  return (
    <Form.Group className={"pb-1 " + styles.formGroup}>
      {props.children}
    </Form.Group>
  );
};

export default FormGroup;
