import React from "react";
import styles from "./SubscribeForm.module.css";
import * as Yup from "yup";
import { Formik } from "formik";
import { addToMailingList } from "../../utils/APICalls";
import PrimaryButton from "../GPButton/PrimaryButton";

const schema = Yup.object({
  email: Yup.string().email("Invalid email"),
});

const SubscribeForm = ({
  className,
  breakOnMobile = false,
  colors,
  id = "subscribeForm",
}) => {
  const onEmailSubmit = (values, formikActions) => {
    addToMailingList({ email: values.email })
      .then((response) => {
        formikActions.setSubmitting(false);
        if (!response._ok) {
          formikActions.setErrors(response.error);
          return;
        }
        formikActions.resetForm();
        formikActions.setStatus({ success: true });
      })
      .catch((error) => {
        console.log(error);
        formikActions.setSubmitting(false);
      });
  };

  const successMessageStyle = {
    color:
      colors && colors.successMessageColor
        ? colors.successMessageColor
        : "white",
  };

  return (
    <Formik
      validationSchema={schema}
      initialValues={{ email: "" }}
      onSubmit={onEmailSubmit}
      initialStatus={{ success: false }}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
        touched,
        handleBlur,
        status,
        setStatus,
      }) => {
        if (status.success && touched.email) {
          //To remove success message after the typing of the new email.
          setStatus({ success: false });
        }
        const isInvalid = touched.email && !!errors.email;
        return (
          <form
            noValidate
            className={
              styles.form +
              (breakOnMobile ? " " + styles.breakable : "") +
              " " +
              className
            }
            onSubmit={handleSubmit}
          >
            <div className={styles.inputContainer}>
              <input
                className={styles.emailInput}
                type="email"
                placeholder="Email"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                id={id}
              />
              {isInvalid && (
                <div className={styles.errorMessage}>{errors.email}</div>
              )}
              {status.success && (
                <div
                  style={successMessageStyle}
                  className={styles.successMessage}
                >
                  Subscribed!
                </div>
              )}
            </div>
            <PrimaryButton className={styles.submitEmail} type="submit">
              Subscribe
            </PrimaryButton>
          </form>
        );
      }}
    </Formik>
  );
};

export default SubscribeForm;
