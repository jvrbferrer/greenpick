import React from "react";
import styles from "./GPButton.module.css";

const GPButton = ({ className, ...props }) => {
  return (
    <button
      {...props}
      type="submit"
      className={styles.button + " " + className}
    >
      {props.children}
    </button>
  );
};

export default GPButton;
