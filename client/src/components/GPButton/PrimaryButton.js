import React from "react";
import GPButton from "./GPButton";
import styles from "./GPButton.module.css";

const PrimaryButton = ({ className, ...props }) => {
  return (
    <GPButton className={styles.primaryButton + " " + className} {...props} />
  );
};

export default PrimaryButton;
