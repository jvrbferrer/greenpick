import React from "react";
import GPButton from "./GPButton";
import styles from "./GPButton.module.css";

const SpecialButton = ({ className, ...props }) => {
  return (
    <GPButton className={styles.specialButton + " " + className} {...props} />
  );
};

export default SpecialButton;
