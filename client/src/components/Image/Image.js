import React from "react";

const Image = ({ imgObject, alt, className, containerClassName, ...props }) => {
  const { oldImages, newImages, sizes, format, defaultImg } = imgObject;

  let type = "";
  let srcSet = "";
  let newSrcSet = "";
  sizes.forEach((val, i) => {
    srcSet = srcSet.concat(`${oldImages[i]} ${sizes[i]}w,`);
    newSrcSet = newSrcSet.concat(`${newImages[i]} ${sizes[i]}w,`);
  });
  //remove trailing commas
  srcSet.slice(0, -1);
  newSrcSet.slice(0, -1);

  if (format === "jpeg" || format === "jpg") {
    type = "image/jpeg";
  } else {
    type = "image/png";
  }

  return (
    <picture className={containerClassName}>
      <source type="image/webp" srcSet={newSrcSet} />
      <source type={type} srcSet={srcSet} />
      <img src={defaultImg} alt={alt} className={className} {...props} />
    </picture>
  );
};

export default Image;
