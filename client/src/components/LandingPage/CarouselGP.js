import React from "react";
import {
  Image1,
  Image2,
  Image4,
  Image5,
  comment1_1,
  comment1_2,
  comment2_1,
  comment3_2,
  comment4_1,
  comment5_1,
  Oval,
  Oval2
} from "../../assets/img/Carousel";
import Comment from "./Comment";
import Carousel from "react-bootstrap/Carousel";
import styles from "./CarouselGP.module.css";
import Image from "../Image/Image";

const CarouselGP = () => {
  return (
    <div className={styles.carouselContainer}>
      <img src={Oval} className={styles.oval1} alt="" />
      <img src={Oval2} className={styles.oval2} alt="" />
      <Carousel interval={3000} className={styles.carousel}>
        <Carousel.Item className={styles.carouselItem}>
          <Image
            imgObject={Image1}
            alt="Brown cardbox stamped with Greenpick logo and a plant on top"
            className={styles.carouselImg}
          />
          <Comment
            imgObject={comment1_1}
            left={"7%"}
            top={"10%"}
            height="auto"
            maxHeight="105px"
            width="auto"
            maxWidth="93%"
            alt="Review praising the eco friendly cardbox"
          />
          <Comment
            imgObject={comment1_2}
            left={"0%"}
            bottom={"30%"}
            width="auto"
            height="auto"
            maxWidth="203px"
            alt="Notification showing that Greenpick planted a tree due to your activity in the platform"
          />
        </Carousel.Item>
        <Carousel.Item className={styles.carouselItem}>
          <Image
            imgObject={Image2}
            alt="Reusable bag from organic cotton full of vegetables"
            className={styles.carouselImg}
          />
          <Comment
            imgObject={comment2_1}
            right={"5%"}
            top={"15%"}
            width="auto"
            height="auto"
            maxHeight="120px"
            maxWidth="95%"
            alt="Review praising the eco friendly bag"
          />
        </Carousel.Item>
        <Carousel.Item className={styles.carouselItem}>
          <Image
            imgObject={Image4}
            alt="Reusable jars in a shop"
            className={styles.carouselImg}
          />
          <Comment
            imgObject={comment4_1}
            left={"5%"}
            bottom={"20%"}
            width="auto"
            height="auto"
            maxHeight="115px"
            maxWidth="95%"
            alt="Review praising zero-waste store products"
          />
          <Comment
            imgObject={comment3_2}
            right={"5%"}
            top={"25%"}
            width="auto"
            height="auto"
            maxWidth="197px"
            alt="Notification: Forest cleaning set in motion thanks to you!"
          />
        </Carousel.Item>
        <Carousel.Item className={styles.carouselItem}>
          <Image
            imgObject={Image5}
            alt="Eco friendly diapers"
            className={styles.carouselImg}
          />
          <Comment
            imgObject={comment5_1}
            right={"5%"}
            top={"20%"}
            width="auto"
            height="auto"
            maxHeight="106px"
            maxWidth="95%"
            alt="Positive Review: My baby loves this eco-friendly diapers"
          />
        </Carousel.Item>
      </Carousel>
    </div>
  );
};

export default CarouselGP;
