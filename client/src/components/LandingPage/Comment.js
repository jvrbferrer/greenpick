import React from "react";
import Image from "../Image/Image";

const Comment = ({
  width,
  height,
  maxWidth,
  maxHeight,
  left,
  right,
  top,
  bottom,
  imgObject,
  alt
}) => {
  let style = {
    position: "absolute",
    width: width,
    height: height,
    maxWidth: maxWidth,
    maxHeight: maxHeight
  };
  if (!!left) {
    style["left"] = left;
  }
  if (!!right) {
    style["right"] = right;
  }
  if (!!top) {
    style["top"] = top;
  }
  if (!!bottom) {
    style["bottom"] = bottom;
  }

  return <Image imgObject={imgObject} style={style} alt={alt} />;
};

export default Comment;
