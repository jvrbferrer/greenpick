import React from "react";
import styles from "./ImageTextTextDiv.module.css";

const ImageTextTextDiv = ({ img, title, description, imgAlt, className }) => {
  return (
    <div className={styles.container + " " + className}>
      <img src={img} alt={imgAlt} className={styles.img} />
      <p className={styles.title}>{title}</p>
      <p className={styles.description}>{description}</p>
    </div>
  );
};

export default ImageTextTextDiv;
