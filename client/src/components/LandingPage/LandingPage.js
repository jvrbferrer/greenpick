import React from "react";
import genericRating from "../../assets/img/generic-rating.svg";
import genericLabel from "../../assets/img/generic-label.svg";
import genericTree from "../../assets/img/generic-tree.svg";
import genericMagnifyingGlass from "../../assets/img/generic-magnifying-glass.svg";
import styles from "./LandingPage.module.css";
import ForestBar from "../ForestBar/ForestBar";
import { useChangeNavbarColor } from "../../utils/Hooks";
import CarouselGP from "./CarouselGP";
import SubscribeForm from "../Forms/SubscribeForm";
import ImageTextTextDiv from "./ImageTextTextDiv";
import Image from "../Image/Image";
import ReviewsPageImg from "../../assets/img/ReviewsPage";
import AppImg from "../../assets/img/App";
import MaxWidthDiv from "../MaxWidthDiv/MaxWidthDiv";
import SocialNetworks from "../SocialNetworks/SocialNetworks";

const LandingPage = ({ setActiveJoinUsModal, setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#F4F6F3"));

  return (
    <div className={styles.mainContainer}>
      <MaxWidthDiv backgroundColor="#f4f6f3" className={styles.firstContainer}>
        <div className={styles.firstFirst}>
          <p className={styles.comingSoonInfo}>Coming soon</p>
          <h1 className={styles.firstTitle}>
            Review, share and discover the sustainability of products and brands
          </h1>
          <h2 className={styles.firstSubtitle}>
            Spread the word on great sustainable products. Be heard by brands.
            Drive change for our planet.
          </h2>
          <SubscribeForm
            breakOnMobile={true}
            colors={{ successMessageColor: "black" }}
            buttonText="Notify me"
            id="notify-me"
          />
        </div>
        <div className={styles.firstSecond}>
          <CarouselGP />
        </div>
      </MaxWidthDiv>
      <div className={styles.secondContainer}>
        <p className={styles.secondTitle}>How it works</p>
        <div>
          <div className={styles.secondTexts}>
            <ImageTextTextDiv
              img={genericRating}
              title="Review the sustainability of products and services"
              description="Your feedback helps others make informed and conscious choices."
            />
            <ImageTextTextDiv
              img={genericTree}
              title="Receive eco-rewards for your reviews"
              description="Support environmental causes – reforestation, recovering plastic from oceans, forests and beaches cleaning – with your eco-reviews."
            />
            <ImageTextTextDiv
              img={genericLabel}
              title="Impact companies’ practices"
              description="Connect with brands to help them improve their environmental footprint."
            />
            <ImageTextTextDiv
              img={genericMagnifyingGlass}
              title="Find greener alternatives for a sustainable lifestyle"
              description="Pick sustainable products and choose brands based on their environmental merits."
            />
          </div>
        </div>
      </div>
      <MaxWidthDiv backgroundColor="#93a086" className={styles.thirdContainer}>
        <div className={styles.thirdFirst}>
          <Image
            imgObject={ReviewsPageImg}
            alt="Environmentally friendly Shampoo"
            className={styles.thirdImg}
          ></Image>
        </div>
        <div className={styles.thirdSecond}>
          <h3 className={styles.thirdTitle}>
            Encouraging a more sustainable life,one eco-review at a time
          </h3>
          <div className={styles.thirdTextsContainer}>
            <p className={styles.thirdText}>
              <span className={styles.bulletStyle}>&bull;</span>Share
              eco-friendly brands and products
            </p>
            <p className={styles.thirdText}>
              <span className={styles.bulletStyle}>&bull;</span>Highlight great
              sustainable initiatives
            </p>
            <p className={styles.thirdText}>
              <span className={styles.bulletStyle}>&bull;</span>Flag
              greenwashing, report overpackaging and other negative practices
            </p>
            <p className={styles.thirdText}>
              <span className={styles.bulletStyle}>&bull;</span>Find information
              on environmental certification and impact of products and services
            </p>
          </div>
        </div>
      </MaxWidthDiv>
      <MaxWidthDiv backgroundColor="white" className={styles.fourthContainer}>
        <div className={styles.fourthFirst}>
          <p className={styles.fourthComingSoon}>Coming soon</p>
          <h3 className={styles.fourthTitle}>
            We’re building a platform to make sustainability accessible to all
          </h3>
          <p className={styles.fourthText}>
            Leave an eco-review right after purchasing a product or finding a
            share-worthy sustainable initiative. Greenpick is your trustful
            partner on your eco-journey.
          </p>
          <SocialNetworks light={true} />
        </div>
        <Image
          imgObject={AppImg}
          alt="Mobile phone showing greenpick app"
          className={styles.fourthImg}
          containerClassName={styles.fourthImgContainer}
        />
      </MaxWidthDiv>
      <ForestBar />
    </div>
  );
};

export default LandingPage;
