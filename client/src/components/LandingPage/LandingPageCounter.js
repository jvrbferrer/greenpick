import React from "react";
import styles from "./LandingPageCounter.module.css";

const LandingPage = ({ counterNumber, counterText, className, ...props }) => {
  return (
    <div className={styles.container + " " + className}>
      <img src={props.imgSrc} alt={props.alt} className={styles.counterImg} />
      <p className={styles.counterNumber}>{counterNumber}</p>
      <p className={styles.counterText}>{counterText}</p>
    </div>
  );
};

export default LandingPage;
