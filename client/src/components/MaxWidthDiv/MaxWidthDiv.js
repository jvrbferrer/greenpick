import React from "react";
import styles from "./MaxWidthDiv.module.css";

const MaxWidthDiv = ({
  backgroundColor = "white",
  maxWidth = "1500px",
  children,
  className,
}) => {
  return (
    <div
      style={{
        backgroundColor,
      }}
      className={styles.outerContainer}
    >
      <div
        style={{
          maxWidth,
        }}
        className={className}
      >
        {" "}
        {children}
      </div>
    </div>
  );
};

export default MaxWidthDiv;
