import React from "react";
import SocialNetworkCard from "./SocialNetworkCard";
import fbLogo from "../../assets/img/SocialMediaSVGs/logo_fb.svg";
import instaLogo from "../../assets/img/SocialMediaSVGs/logo_insta.svg";
import redditLogo from "../../assets/img/SocialMediaSVGs/logo_reddit.svg";
import styles from "./Modal.module.css";
import Modal from "../Modal/Modal";

const JoinUsModal = ({ closeModal }) => {
  return (
    <Modal
      closeModal={closeModal}
      contentStyle={"d-flex flex-column " + styles.modalContent}
    >
      <div
        className={"d-flex flex-row justify-content-between align-items-start"}
      >
        <p className={styles.shareText}>Join us</p>
        <svg
          viewBox="0 0 16 16"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          onClick={closeModal}
          className={styles.closeModalImg}
        >
          <path
            fillRule="evenoLindd"
            d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z"
            clipRule="evenodd"
          />
          <path
            fillRule="evenodd"
            d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z"
            clipRule="evenodd"
          />
        </svg>
      </div>
      <a
        href="https://www.facebook.com/Greenpick-107553714341307"
        onClick={() => closeModal()}
        className={styles.link}
      >
        <SocialNetworkCard
          img={fbLogo}
          text="Facebook"
          className={styles.bottomBorder}
        />
      </a>
      <a
        href="https://www.instagram.com/greenpick.co/"
        onClick={() => closeModal()}
        className={styles.link}
      >
        <SocialNetworkCard
          img={instaLogo}
          text="Instagram"
          className={styles.bottomBorder}
        />
      </a>
      <a
        href="https://www.reddit.com/r/Greenpick"
        onClick={() => closeModal()}
        className={styles.link}
      >
        <SocialNetworkCard img={redditLogo} text="Reddit" />
      </a>
    </Modal>
  );
};

export default JoinUsModal;
