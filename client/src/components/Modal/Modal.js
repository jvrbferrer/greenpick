import React, { useRef, useEffect } from "react";
import styles from "./Modal.module.css";

const Modal = ({ closeModal, children, contentStyle }) => {
  const modalRef = useRef(null);

  useEffect(() => {
    const handleClick = event => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        closeModal();
      }
    };

    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, [modalRef, closeModal]);

  return (
    <div className={styles.modalContainer}>
      <div className={contentStyle} ref={modalRef}>
        {children}
      </div>
    </div>
  );
};

export default Modal;
