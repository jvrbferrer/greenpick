import React from "react";
import SocialNetworkCard from "./SocialNetworkCard";
import fbLogo from "../../assets/img/SocialMediaSVGs/logo_fb.svg";
import twitterLogo from "../../assets/img/SocialMediaSVGs/logo_twitter.svg";
import whatsAppLogo from "../../assets/img/SocialMediaSVGs/logo_whatsapp.svg";
import copyClipboardIcon from "../../assets/img/SocialMediaSVGs/copy_clipboard.svg";
import styles from "./Modal.module.css";
import Modal from "../Modal/Modal";

const ShareWithModal = ({ linkToShare, closeShareModal }) => {
  const copyToClipboard = text => {
    const el = document.createElement("textarea");
    el.value = text;
    el.setAttribute("readonly", "");
    el.style.position = "absolute";
    el.style.left = "-9999px";
    document.body.appendChild(el);
    el.select();
    el.setSelectionRange(0, 99999);
    document.execCommand("copy");
    document.body.removeChild(el);
    alert("Copied to clipboard");
  };

  return (
    <Modal
      closeModal={closeShareModal}
      contentStyle={"d-flex flex-column " + styles.modalContent}
    >
      <div
        className={"d-flex flex-row justify-content-between align-items-start"}
      >
        <p className={styles.shareText}>Share</p>
        <svg
          viewBox="0 0 16 16"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          onClick={closeShareModal}
          className={styles.closeModalImg}
        >
          <path
            fillRule="evenoLindd"
            d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z"
            clipRule="evenodd"
          />
          <path
            fillRule="evenodd"
            d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z"
            clipRule="evenodd"
          />
        </svg>
      </div>
      <a
        onClick={e => {
          e.preventDefault();
          window.FB.ui(
            {
              method: "share",
              href: linkToShare
            },
            response => {}
          );
        }}
        href={linkToShare}
        className={styles.link}
      >
        <SocialNetworkCard
          img={fbLogo}
          text="Facebook"
          className={styles.bottomBorder}
        />
      </a>
      <a
        href={"https://twitter.com/share?ref_src=" + linkToShare}
        target="_blank"
        rel="noopener noreferrer"
        className={styles.link}
      >
        <SocialNetworkCard
          img={twitterLogo}
          text="Twitter"
          className={styles.bottomBorder}
        />
      </a>
      <a
        href={"https://api.whatsapp.com/send?text=" + linkToShare}
        target="_blank"
        rel="noopener noreferrer"
        className={styles.link}
      >
        <SocialNetworkCard
          img={whatsAppLogo}
          text="WhatsApp"
          className={styles.bottomBorder}
        />
      </a>
      <span onClick={() => copyToClipboard(linkToShare)}>
        <SocialNetworkCard img={copyClipboardIcon} text="Copy to clipboard" />
      </span>
    </Modal>
  );
};

export default ShareWithModal;
