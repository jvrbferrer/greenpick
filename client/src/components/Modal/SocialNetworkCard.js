import React from "react";
import styles from "./Modal.module.css";

const SocialNetworkCard = (props) => {
  return (
    <div
      className={
        "d-flex flex-row align-items-center " +
        styles.socialNetworkCard +
        " " +
        props.className
      }
    >
      <img src={props.img} alt="" className={styles.socialNetworkCardImg} />
      <p className={styles.socialNetworkCardText}>{props.text}</p>
    </div>
  );
};

export default SocialNetworkCard;
