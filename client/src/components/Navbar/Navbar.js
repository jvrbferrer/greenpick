import React, { useState } from "react";
import { CSSTransition } from "react-transition-group";
import NavbarBS from "react-bootstrap/Navbar";
import SpecialButton from "../GPButton/SpecialButton";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import logoName from "../../assets/img/group.svg";
import styles from "./Navbar.module.css";
import NavbarLink from "./NavbarLink";
import Sidenav from "./Sidenav";
import openSideNavButton from "../../assets/img/openSideNavButton.svg";

const Navbar = ({ setActiveJoinUsModal, ...props }) => {
  const [activeSideNav, setActiveSideNav] = useState(false);
  return (
    <NavbarBS
      className={styles.navbar}
      style={{ backgroundColor: props.backgroundColor }}
    >
      <Link to="/">
        <NavbarBS.Brand className="ml-sm-4 ml-0">
          <img
            src={logoName}
            alt="Greenpick logo"
            className={"d-inline-block align-bottom " + styles.navbarImage}
          />
        </NavbarBS.Brand>
      </Link>

      <CSSTransition
        in={activeSideNav}
        timeout={500}
        classNames={{
          enter: styles.sidenavEnter,
          enterActive: styles.sidenavEnterActive,
          exit: styles.sidenavExit,
          exitActive: styles.sidenavExitActive,
        }}
        mountOnEnter
        unmountOnExit
      >
        <div className={styles.sidenav}>
          <Sidenav
            closeSidenav={() => setActiveSideNav(false)}
            setActiveJoinUsModal={setActiveJoinUsModal}
          />
        </div>
      </CSSTransition>

      <img
        src={openSideNavButton}
        alt="Open sidenav icon"
        className={"d-block d-md-none ml-auto " + styles.openSideNavButton}
        onClick={() => setActiveSideNav(true)}
      />

      <div className="d-none d-md-flex justify-content-end ml-auto align-items-center">
        <Nav className="ml-auto mr-2">
          {/* <NavbarLink to="/collect-reward/">Collect reward</NavbarLink> */}
          <NavbarLink to="/about/">About</NavbarLink>
          <NavbarLink to="/contact/">Contact</NavbarLink>
        </Nav>
        <a href="/#notify-me">
          <SpecialButton className={"ml-4 mr-3 " + styles.joinBtn}>
            Subscribe
          </SpecialButton>
        </a>
      </div>
    </NavbarBS>
  );
};

export default Navbar;
