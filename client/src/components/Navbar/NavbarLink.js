import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./Navbar.module.css";

const NavbarLink = props => {
  return (
    <NavLink
      {...props}
      activeClassName={styles.activeNavbarLink}
      className={"mx-3 " + styles.navbarLink}
    />
  );
};

export default NavbarLink;
