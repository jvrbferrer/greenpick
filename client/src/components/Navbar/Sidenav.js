import React from "react";
import styles from "./Navbar.module.css";
import logoNameWhite from "../../assets/img/group-white.svg";
import SocialNetworks from "../SocialNetworks/SocialNetworks";
import { Link } from "react-router-dom";

const Sidenav = (props) => {
  return (
    <div className={"d-flex flex-column " + styles.sidenavContainer}>
      <div className="d-flex flex-row align-items-center justify-content-between">
        <img
          src={logoNameWhite}
          alt="Greenpick logo"
          className={styles.sidenavBrand}
        />
        <svg
          className={styles.sidenavCloseButton}
          viewBox="0 0 16 16"
          fill="currentColor"
          xmlns="http://www.w3.org/2000/svg"
          onClick={props.closeSidenav}
        >
          <path
            fillRule="evenoLindd"
            d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z"
            clipRule="evenodd"
          />
          <path
            fillRule="evenodd"
            d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z"
            clipRule="evenodd"
          />
        </svg>
      </div>
      <div className={"d-flex flex-column " + styles.sidenavLinksContainer}>
        <Link to="/" onClick={props.closeSidenav}>
          <p className={styles.sidenavLink}>Home</p>
        </Link>
        {/* <Link to="/collect-reward" onClick={props.closeSidenav}>
          <p className={styles.sidenavLink}>Collect reward</p>
        </Link> */}
        <Link to="/about" onClick={props.closeSidenav}>
          <p className={styles.sidenavLink}>About</p>
        </Link>
        <Link to="/contact" onClick={props.closeSidenav}>
          <p className={styles.sidenavLink}>Contact</p>
        </Link>
        <a href="/#notify-me" onClick={props.closeSidenav}>
          <p className={styles.sidenavLink}>Notify me</p>
        </a>
      </div>
      <SocialNetworks className="mt-auto mb-3" />
    </div>
  );
};

export default Sidenav;
