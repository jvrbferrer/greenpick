import React from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import PrimaryButton from "../GPButton/PrimaryButton";
import ForestBar from "../ForestBar/ForestBar";
import styles from "./NotFoundPage.module.css";
import { useChangeNavbarColor } from "../../utils/Hooks";

const NotFoundPage = ({ setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#FFFFFF"));

  return (
    <>
      <Container fluid className="text-center py-5">
        <p className={styles.oops}>Oops!</p>
        <p className={styles.text}>
          We can't seem to find the page you're looking for
        </p>
        <p className={styles.errorCode}>Error code: 404</p>
        <Link to="/">
          <PrimaryButton className={styles.button}>
            Go to homepage
          </PrimaryButton>
        </Link>
      </Container>
      <ForestBar />
    </>
  );
};

export default NotFoundPage;
