import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useParams, useLocation } from "react-router-dom";
import TreePrizePage from "./TreePrizePage";
import styles from "./PrizePages.module.css";
import ForestBar from "../ForestBar/ForestBar";
import { useChangeNavbarColor } from "../../utils/Hooks";
import { getCodeInfo } from "../../utils/APICalls";
import NotFoundPage from "../NotFoundPage/NotFoundPage";

const PrizePage = ({ setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#FFFFFF"));
  const params = useParams();
  const location = useLocation();
  const code = !!params.code ? params.code : location.state.codeString;

  const [codeInfo, setCodeInfo] = useState({
    name: location.state ? location.state.name : null,
    prize: location.state ? location.state.prize : null,
    fetching: !!params.code, //If a code is present in the URL then we'll fetch the info of that code
    failedToFetch: false,
  });

  useEffect(() => {
    if (!!params.code) {
      getCodeInfo({ codeString: params.code })
        .then((response) => {
          if (response._ok) {
            setCodeInfo((curState) => ({
              name: response.name,
              prize: response.prize,
              fetching: false,
              failedToFetch: false,
            }));
          } else {
            setCodeInfo((curState) => ({
              ...curState,
              fetching: false,
              failedToFetch: true,
            }));
          }
        })
        .catch((error) => {
          setCodeInfo((curState) => ({
            ...curState,
            fetching: false,
            failedToFetch: true,
          }));
        });
    }
  }, [params.code]);

  let PrizeScreen = null;
  if (codeInfo.prize === "tree") {
    PrizeScreen = TreePrizePage;
  } else if (codeInfo.failedToFetch || !codeInfo.fetching) {
    return <NotFoundPage setHeaderColor={setHeaderColor} />;
  } else {
    //Hasn't fetched yet so don't render anything
    return null;
  }

  return (
    <>
      <PrizeScreen name={codeInfo.name} code={code} />
      <Container fluid>
        <Row>
          <Col>
            <p className={styles.bottomCallForAction}>
              Keep reviewing, win more rewards, improve your ecological
              footprint
            </p>
          </Col>
        </Row>
      </Container>
      <ForestBar />
    </>
  );
};

export default PrizePage;
