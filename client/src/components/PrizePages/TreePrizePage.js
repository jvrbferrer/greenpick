import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Link } from "react-router-dom";
import PrimaryButton from "../GPButton/PrimaryButton";
import TreeImg from "../../assets/img/generic-tree.svg";
import ShareWithModal from "../Modal/ShareWithModal";
import Image from "../Image/Image";
import styles from "./PrizePages.module.css";
import ForestImg from "../../assets/img/Forest";
import SoilImg from "../../assets/img/Soil";

const TreePrizePage = props => {
  const [isOpenShareWithModal, setIsOpenShareWithModal] = useState(false);
  const nameToPresent = props.name === "" ? "kind stranger" : props.name;
  const linkToShare = `${window.location.origin}/reward/${props.code}`;

  return (
    <>
      {isOpenShareWithModal && (
        <ShareWithModal
          closeShareModal={() => setIsOpenShareWithModal(false)}
          linkToShare={linkToShare}
        />
      )}
      <Container fluid className={"pb-4 pt-5 " + styles.mainContainer}>
        <Row className="mb-5 justify-content-center">
          <Col xs="auto">
            <img src={TreeImg} alt="Tree" />
          </Col>
        </Row>
        <Row className="text-center">
          <Col md={{ offset: 2, span: 8 }}>
            <div className={styles.textBox}>
              <p className={styles.greetingText}>
                Your reward, {nameToPresent}
              </p>
              <p className={"px-5 " + styles.prizeTitle}>
                One new tree planted on your behalf
              </p>
              <p className={styles.prizeText}>
                Your reviews are crucial for Greepinck’s goal of building
                environmentally sustainable communities.
              </p>
              <p className={styles.prizeText}>
                Greenpick is rewarding your effort with one new tree planted on
                your&nbsp;behalf.
              </p>
            </div>
            <PrimaryButton
              className={"mt-4 " + styles.shareButton}
              onClick={setIsOpenShareWithModal.bind(true)}
            >
              Share
            </PrimaryButton>
          </Col>
        </Row>
        <Row
          className={
            "justify-content-center d-flex flex-column align-items-center " +
            styles.separator
          }
        >
          <p className={styles.text}>Scroll down</p>
          <div className={styles.verticalLine}></div>
        </Row>
        <Row className={styles.whatRow}>
          <Col
            md={{ offset: 0, span: 6 }}
            xs={{ offset: 0, span: 12 }}
            className={"d-flex"}
          >
            <div className={styles.whatText}>
              <p className={"mb-1 " + styles.questionText}>
                What can a tree do?
              </p>
              <div className={"mb-3 " + styles.horizontalLine}></div>
              <p className={styles.text}>
                A lot! The trees that you help plant bring many benefits to the
                environment:
              </p>
              <p className={styles.listElement}>
                <span className={styles.bulletStyle}>&bull;</span> Sequester
                carbon, thus reducing greenhouse gas emissions{" "}
              </p>
              <p className={styles.listElement}>
                <span className={styles.bulletStyle}>&bull;</span> Improve the
                air quality{" "}
              </p>
              <p className={styles.listElement}>
                <span className={styles.bulletStyle}>&bull;</span> Help combat
                wind and water erosion
              </p>
              <p className={styles.listElement}>
                <span className={styles.bulletStyle}>&bull;</span> May
                contribute to increased rainfal{" "}
              </p>
              <p className={styles.listElement}>
                <span className={styles.bulletStyle}>&bull;</span> Enhance
                biodiversity and restore habitat for native animals
              </p>
              <p className={styles.text}>
                Greenpick supports biodiverse conservation plantings using trees
                and shrubs thatare native to regions.
              </p>
            </div>
          </Col>
          <Col
            md={{ offset: 0, span: 6 }}
            xs={{ offset: 0, span: 11 }}
            className={"d-flex"}
          >
            <div
              className={"mt-md-auto mb-md-auto mt-2 " + styles.imgContainer}
            >
              <Image
                className={styles.imgForest}
                imgObject={ForestImg}
                alt="Forest"
              />
            </div>
          </Col>
        </Row>
        <Row className={"mt-1 " + styles.rowHow}>
          <Col
            md={{ offset: 0, span: 6, order: 1 }}
            xs={{ offset: 0, span: 11, order: 2 }}
          >
            <div className={"d-flex mt-md-0 mt-2 " + styles.imgContainer}>
              <Image
                imgObject={SoilImg}
                className={styles.imgForest}
                alt="Soil with newborn stem"
              />
            </div>
          </Col>
          <Col
            md={{ offset: 0, span: 6, order: 2 }}
            xs={{ offset: 0, span: 12, order: 1 }}
            className="d-flex"
          >
            <div className={styles.whatText}>
              <p className={"mb-1 " + styles.questionText}>How does it work?</p>
              <div className={"mb-3 " + styles.horizontalLine}></div>
              <p className={styles.text}>
                Greenpick uses revenues to fund the green rewards system. A
                reward certificate will be sent to you, highlighting your
                contribution to the environment.
              </p>
              <p className={styles.text}>
                By joining Greenpick you are not only contributing to create
                awareness with your reviews, as brands are notified about your
                feedback, but also creating change by supporting environmental
                causes with the rewards you collect.
              </p>
              <Link to="/about/">
                <PrimaryButton className={"mt-1 " + styles.readMoreButton}>
                  Read more
                </PrimaryButton>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default TreePrizePage;
