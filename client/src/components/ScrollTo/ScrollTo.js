import { useEffect } from "react";
import { useLocation } from "react-router";

const ScrollTo = () => {
  const location = useLocation();
  useEffect(() => {
    if (location.hash !== "") {
      return;
    }
    window.scrollTo(0, 0);
  }, [location]);
  //Everytime there is a change in scroll Location we change the scroll location
  return null;
};

export default ScrollTo;
