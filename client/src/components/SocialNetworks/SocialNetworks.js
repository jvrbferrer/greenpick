import React from "react";
import fbThumb from "../../assets/img/fbthumbnail.svg";
import instaThumb from "../../assets/img/instathumbnail.svg";
// import redditThumb from "../../assets/img/redditthumbnail.svg";
import fbThumbLight from "../../assets/img/SocialMediaSVGs/logo_fb_light.svg";
import instaThumbLight from "../../assets/img/SocialMediaSVGs/logo_insta_light.svg";

const SocialNetworks = ({ className, light = false }) => {
  const fbLogoSrc = light ? fbThumbLight : fbThumb;
  const instaLogoSrc = light ? instaThumbLight : instaThumb;
  return (
    <div className={"d-flex flex-row " + className}>
      <a
        href="https://www.instagram.com/greenpick.co/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img className="mr-4" src={instaLogoSrc} alt="instagram page" />
      </a>
      <a
        href="https://www.facebook.com/Greenpick-107553714341307"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img className="mr-4" src={fbLogoSrc} alt="facebook page" />
      </a>
      {/* <a
        href="https://www.reddit.com/r/Greenpick"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img src={redditThumb} alt="reddit page" />
      </a> */}
    </div>
  );
};

export default SocialNetworks;
