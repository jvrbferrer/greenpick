import React from "react";
import styles from "./Supports.module.css";
import Image from "../Image/Image";
import iapmeiImg from "../../assets/img/Supports/IAPMEI";
import lisboa2020Img from "../../assets/img/Supports/Lisboa2020";
import portugal2020Img from "../../assets/img/Supports/Portugal2020";
import UEImg from "../../assets/img/Supports/UE";

const Supports = () => {
  return (
    <>
      <p className={"ml-xl-5 ml-3 " + styles.text}>
        Project supported by StartUp Voucher and co-financed by:
      </p>
      <div className={"ml-xl-5 ml-3 " + styles.container}>
        <div className={styles.imgContainer}>
          <Image
            className={styles.supportImg}
            imgObject={iapmeiImg}
            alt="Iapmei logo"
          />
        </div>
        <div className={styles.imgContainer}>
          <Image
            className={styles.supportImg}
            imgObject={lisboa2020Img}
            alt="Lisboa 2020 logo"
          />
        </div>
        <div className={styles.imgContainer}>
          <Image
            className={styles.supportImg}
            imgObject={portugal2020Img}
            alt="Portugal 2020 logo"
          />
        </div>
        <div className={styles.imgContainer}>
          <Image
            className={styles.supportImg}
            imgObject={UEImg}
            alt="European Union logo"
          />
        </div>
      </div>
    </>
  );
};

export default Supports;
