import React from "react";
import styles from "./TermsAndConditions.module.css";
import { Link } from "react-router-dom";
import { useChangeNavbarColor } from "../../utils/Hooks";

const TermsAndConditionsPage = ({ setHeaderColor }) => {
  useChangeNavbarColor(() => setHeaderColor("#F4F6F3"));
  return (
    <div className={styles.background}>
      <div className={styles.mainContainer}>
        <p className={styles.title}>Terms & conditions</p>
        <p className={styles.text}>
          Greenpick is working to give you an open-to-all eco-conscious reviews
          platform where you can review, share and discover the sustainability
          of products and services and connect with businesses to help them
          improve their ecological footprint.
        </p>
        <p className={styles.text}>
          Here you can find all our important guidelines and policies.
        </p>
        <p className={styles.text}>
          We kindly ask you to respect and follow these guidelines and policies,
          to keep Greenpick a collaborative and trustworthy platform for
          everyone.
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>
          <a className={styles.text} href="/terms&conditions/#guidelines">
            <u>Guidelines</u>
          </a>
          <br />
          <span className={styles.bulletStyle}>&bull;</span>
          <a className={styles.text} href="/terms&conditions/#privacyPolicy">
            <u>Privacy Policy</u>
          </a>
          <br />
          <span className={styles.bulletStyle}>&bull;</span>
          <a
            className={styles.text}
            href="/terms&conditions/#termsAndConditions"
          >
            <u>Terms & Conditions</u>
          </a>
        </p>
        <p className={styles.text}>
          Thank you for joining Greenpick. Together we will make a decisive
          effort towards the sustainability of our planet!
        </p>
        <p className={styles.subtitle} id="guidelines">
          Guidelines
        </p>
        <p className={styles.text}>
          <i className={styles.italic}>December 2020</i>
        </p>
        <p className={styles.text}>
          We kindly ask you to respect and follow these guidelines, together
          with our{" "}
          <a
            className={styles.text}
            href="/terms&conditions/#termsAndConditions"
          >
            <u>Terms of Use</u>
          </a>
          , to ensure that Greenpick remains a collaborative and trustworthy
          platform for everyone.
        </p>
        <p className={styles.text}>Here are the guidelines for our platform:</p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span> You can post
          reviews on the sustainability of products &amp; services.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Once you post your
          review the following information is disclosed: your review(s), your
          username and location (country).
          <br />
          <span className={styles.bulletStyle}>&bull;</span> You can choose to
          post a review anonymously, in which case your username will not be
          disclosed and the review will not appear in your public profile page.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> The companies you
          review will be notified by Greenpick and receive information about
          your review. If you have chosen to post a review anonymously, the
          company reviewed will receive the content of the review and the
          location, but your username will not be disclosed in any circumstance.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> After you have
          posted 10 reviews from the same account, a reward code will be sent to
          you by our team via private message. Insert the code{" "}
          <Link className={styles.text} to="/collect-reward">
            <u>here</u>
          </Link>{" "}
          and collect your eco-reward.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Eco-rewards are
          limited to existing stock: a maximum of 2000 eco-rewards will be
          attributed, meaning that only the first 2000 users that post 10
          eco-reviews from the same account and introduce a valid code on the
          website will receive eco-rewards.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Your reviews are
          owned by you. That means you can edit, update or delete your
          reviews at any time. You are encouraged to update an existing review
          if you want to add more information about a particular
          product/service. And while it is fine to write more than one
          eco-review for a business, please do not bombard a business with more
          than one review for the same product/service.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Everyone can
          provide feedback to reviews. Since reviews are based on common-sense
          environmentally friendly practices – that are often biased – and the
          determination of the environmental impact of products and services is
          an extremely complex subject, we strongly incentivize that those with
          expertise on sustainability matters, as well as the interested
          companies, intervene and provide feedback so as to tackle inaccurate
          reviews.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Everyone can flag
          reviews. Only flag a review if you genuinely believe there is a
          problem, and please be fair and consistent. Our team will check
          flagged reviews to see if they breach our guidelines and take measures
          accordingly.
          <br />
        </p>
        <p className={styles.text}>
          Rules we appreciate you to follow when submitting an eco-review on
          Facebook and Reddit:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>You post a review on
          the sustainability of a product/service. Your review is expected to
          give other people and companies valuable up-to-date feedback on the
          sustainability of the product/service.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Tag the company that
          provides the product/service. Please double check the company and
          country domain you are reviewing in order to avoid common mistakes.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>If your review aims
          to report greenwashing from a brand, please tag #greenwashing;
          <br />
          <span className={styles.bulletStyle}>&bull;</span>If someone flags
          your review for breaking our rules, we may temporarily hide it and ask
          you to make changes. We will give you the opportunity to update your
          review within our guidelines so it can stay on Greenpick. But if you
          seriously misuse our platform we can remove your reviews and/or block
          your account. If your account is blocked, your reviews will be
          deleted.
          <br />
        </p>
        <p className={styles.subtitle} id="privacyPolicy">
          Privacy Policy
        </p>
        <p className={styles.text}>
          <i className={styles.italic}>December 2020</i>
          <br />
          <i className={styles.italic}>(Version 2.0)</i>
        </p>
        <p className={styles.text}>
          When you write a review on Greenpick or otherwise use our website, we
          collect and process personal data about you. This document helps you
          understand what personal data we collect about you, how we collect it,
          what we use it for, and what rights you have regarding your personal
          data.
        </p>
        <p className={styles.subsubtitle}>
          Personal data: What we collect and what we use it for
        </p>
        <p className={styles.text}>
          When you subscribe our service in our website, the following personal
          data about you (the “Master Data”):
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>Name and/or
          username.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>E-mail address.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Location and
          country.
        </p>
        <p className={styles.subsubtitle}>Information in eco-reviews</p>
        <p className={styles.text}>
          When you write a review on the sustainability of a company, a product
          or a service, we collect the information you put in your review. This
          includes:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>Which company you
          review.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>The type of product
          or service your review concerns;
          <br />
          <span className={styles.bulletStyle}>&bull;</span>The headline and
          content of your review;
          <br />
          <span className={styles.bulletStyle}>&bull;</span>The location;
          <br />
          <span className={styles.bulletStyle}>&bull;</span>The date of your
          review and, if you update the review, the date on which it is edited.;
        </p>
        <p className={styles.text}>
          If we ask you to provide information or documentation verifying your
          product/service experience and you send us such documentation, we will
          collect and process the documentation for the purpose(s) stated in the
          request. Please do not share any sensitive personal documents or
          information with us, either concerning yourself or others.
        </p>
        <p className={styles.subsubtitle}>
          Information about views, likes and how useful your review is for
          others
        </p>
        <p className={styles.text}>
          When you write a review on our platform, people can show that they
          find your review useful, for example by “liking it”, and we collect
          this information. We also collect information on the feedback other
          people provide to your review. If you “like” another person’s review,
          we will also collect that information. We will also register the
          number of views your review receives on our website.
        </p>
        <p className={styles.subsubtitle}>Notifications</p>
        <p className={styles.text}>
          If you notify us about a review that you believe violates our 
          <a className={styles.text} href="/terms&conditions/#guidelines">
            <u>Guidelines</u>
          </a>
          , we collect the information you provide in your notification to us.
          It can include which review you notified us about, the reason for your
          notification, the date of your notification, etc.
        </p>
        <p className={styles.subsubtitle}>
          Your IP address, browser settings and location
        </p>
        <p className={styles.text}>
          When you visit the website, we register your computer’s IP address and
          browser settings. The IP address is the numerical address of the
          computer used to visit the website. Browser settings can include the
          type of browser you use, browser language, and time zone.
        </p>
        <p className={styles.subsubtitle}>Newsletters and digest emails</p>
        <p className={styles.text}>
          When you subscribe to receive our newsletters or similar e-mails we
          collect your e-mail address. If you no longer wish to receive our
          newsletters or similar e-mails, you can unsubscribe or you can contact
          us at inbox@greenpick.co.
        </p>
        <p className={styles.subsubtitle}>
          For what purposes do we use your personal data?
        </p>
        <p className={styles.text}>
          We will use the information you provide to us to:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>
          Provide our services to you, including displaying your reviews,
          allowing you to collect your eco-rewards, and providing you with
          access to our website.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Identify you as a
          user when you access the website and re-visit the website.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Verify the
          legitimacy of your reviews.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Improve the website
          and our services.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Invite you to leave
          more reviews.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Respond to your
          questions and provide related customer service.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Pass on a message
          from the company you reviewed.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Contact you if your
          review is flagged by other users or companies and, if necessary, ask
          you to provide documentation to verify your review.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>
          Send you our newsletters.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Inform you when
          other users find your review helpful or otherwise provide feedback
          concerning your review.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>
          Facilitate the social sharing function, including giving you the
          option to connect with members of your network who are both users of
          Greenpick and users of one or more social networks.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Display which
          members of your social networks are users of Greenpick in order to
          increase confidence in reviews and to create a better user experience
          on our website.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Engage in various
          internal purposes, such as data analysis, fraud monitoring and
          prevention, developing new products and services, improving or
          modifying the website, identifying usage trends.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Comply with legal
          requirements and legal process, requests from public and governmental
          authorities, relevant industry standards and our internal policies.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Enforce our 
          <a
            className={styles.text}
            href="/terms&conditions/#termsAndConditions"
          >
            <u>Terms And Conditions</u>
          </a>
          .
        </p>
        <p className={styles.text}>
          We will also use the information in other ways for which we provide
          specific notice at the time of collection.
        </p>
        <p className={styles.subsubtitle}>
          Disclosure of personal data on the website
        </p>
        <p className={styles.text}>
          We are an open-to-all eco-reviews platform and we share your review on
          our website so that others can read about your experience regarding
          the sustainability of a specific product, service or company.
        </p>
        <p className={styles.text}>
          When you write an eco-review on our platform, information such as your
          profile photo, your name and location are used to identify you on the
          website. The companies you review will also receive information on
          your review. You can choose to publish a review anonymously in which
          case your personal data will not appear in the review.
        </p>
        <p className={styles.subsubtitle}>
          Disclosure to other services, websites and companies
        </p>
        <p className={styles.text}>
          One of Greenpick&#39;s main goals is to raise awareness on the
          sustainability of products and services, so it is important to
          increase the exposure and availability of eco-reviews on the website.
          We therefore permit other services to show reviews visible on the
          website. This increases the potential audience for your reviews.
        </p>
        <p className={styles.text}>
          The categories of third party services and companies who can show your
          review(s) are:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>Search engines,
          including Google and Bing.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Companies reviewed
          on Greenpick.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Consumer portals and
          business portals, shopping guides, etc.
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Other similar
          websites where, in Greenpick’s assessment, it will be relevant for
          users to search for eco-reviews.
        </p>
        <p className={styles.subsubtitle}>Other disclosures</p>
        <p className={styles.text}>
          In addition to the above, we disclose your personal data to the
          following parties and in the following circumstances:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span> To comply with laws
          or to respond to claims, legal process (including but not limited to
          subpoenas and court orders) and requests from public and government
          authorities.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> To cooperate with
          regulatory bodies and government authorities in connection with
          investigations or case referrals.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> To third parties in
          connection with enforcement of our Terms &amp; Conditions and User
          Guidelines.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> To third parties in
          order for us to protect our operations.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> To third parties to
          protect our rights, privacy, safety or property and/or that of you or
          others.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> To third parties in
          order for us to pursue available remedies, or limit damages that we
          may sustain.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> To third parties in
          order for us to investigate, prevent or take action regarding
          suspected or actual prohibited activities, including but not limited
          to fraud and misuse of our website.
        </p>
        <p className={styles.subsubtitle}>
          Data controller: Information for which we are the data controller
        </p>
        <p className={styles.text}>
          We are the data controller of the data and other additional
          information you enter on our website, when you collect your
          eco-rewards, which includes your name, e-mail address and location, as
          well as the registration of your IP address.
        </p>
        <p className={styles.text}>
          We are also the data controller of the information which is disclosed
          to other services.
        </p>
        <p className={styles.text}>
          Portugal’s data protection laws govern collection of personal data by
          Portugal in the EU.
        </p>
        <p className={styles.subsubtitle}>
          Data controller: Information for which you are the data controller
        </p>
        <p className={styles.text}>
          You are the data controller for the personal data and other additional
          information disclosed when you post your eco-reviews on Greenpick’s
          website.
        </p>
        <p className={styles.subsubtitle}>Links to websites</p>
        <p className={styles.text}>
          Our website contains links to other websites. Our inclusion of such
          links does not imply that we endorse those websites. We do not control
          the content of those third party websites, and assume no
          responsibility for the third party or their policies or practices.
        </p>
        <p className={styles.text}>
          We encourage you to review the privacy policies for these third party
          websites because their procedures for collecting, handling and
          processing personal data will be different from ours.
        </p>
        <p className={styles.subsubtitle}>Data processors</p>
        <p className={styles.text}>
          The website is controlled and operated by us from Portugal. If you
          access or use the website from a location outside of Portugal, you
          consent to the transfer, storage and processing of your personal data
          in Portugal.
        </p>
        <p className={styles.subsubtitle}>Data retention</p>
        <p className={styles.text}>
          We keep the personal data you provide, including your reviews, for as
          long as needed to provide you with our services. We will delete this
          information upon your request and we will only save a log with the
          following information: your name, e-mail address and the date of the
          deletion. We will keep the log for 2 years. All other information will
          be deleted.
        </p>
        <p className={styles.text}>
          In some cases, we choose to retain certain information (e.g. visits to
          our website) in an anonymized or aggregated form.
        </p>
        <p className={styles.subsubtitle}>Security measures</p>
        <p className={styles.text}>
          We use reasonable organizational, technical and administrative
          measures to protect your personal data. However, since the internet is
          not a 100% secure environment, we cannot ensure or warrant the
          security of the information you transmit to us. E-mails sent via the
          website may not be encrypted, and we therefore advise you not to
          include any confidential information in your e-mails to us.
        </p>
        <p className={styles.text}>
          We are always working to improve our security practices and we will
          update this information as these practices evolve over time.
        </p>
        <p className={styles.subsubtitle}>Cookies</p>
        <p className={styles.text}>
          The website uses cookies and similar technologies (“Cookies”). By
          using our website, you accept that we use Cookies as described below.
        </p>
        <p className={styles.subsubtitle}>What types of Cookies do we use?</p>
        <p className={styles.text}>
          Cookies are small pieces of information that the website places on
          your computer's hard disk, on your tablet or on your smartphone. Note
          that HTML5 introduced Web Storage that has a similar nature to
          Cookies, and that we therefore consider that as a Cookie in the
          following.
        </p>
        <p className={styles.text}>
          Cookies contain information that the website uses to make the
          communication between you and your web browser more efficient. Cookies
          identify your computer or device rather than you as an individual
          user.
        </p>
        <p className={styles.text}>
          We use session cookies, persistent cookies, HTML5 sessionStorage and
          HTML5 localStorage session cookies and HTML5 sessionStorage objects
          are temporary in nature and are deleted when you exit your web
          browser. Persistent cookies are permanent in nature and are stored and
          remain on your computer until they are deleted. Persistent cookies
          expire or auto delete after a certain period of time, which is set per
          cookie, but are renewed each time you visit the website. HTML5
          localStorage objects are permanent in nature and remain on your
          computer until they are deleted.
        </p>
        <p className={styles.subsubtitle}>What do we use Cookies for?</p>
        <p className={styles.text}>We use Cookies for:</p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span> Generating
          statistics: Measuring website traffic such as the number of visits to
          the website, which domains the visitors come from, which pages they
          visit on the website and in which overall geographical areas the
          visitors are located.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Monitoring website
          performance and your use of our website: Monitoring the performance of
          the website and how you use our website.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Authentication and
          improving the functionality of our website: Optimizing your experience
          with the website and remembering information about your browser and
          preferences.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Connecting you to
          social networks: We give you the option of connecting with social
          networks, such as Facebook and Reddit.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Quality assurance:
          Ensuring the quality of reviews and to prevent misuse or
          irregularities in connection with writing reviews and using the
          website.
        </p>
        <p className={styles.subsubtitle}>Third party Cookies</p>
        <p className={styles.text}>
          Our website uses cookies from the following third parties:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span> Google
          Analytics: For statistical purposes. You can decline cookies from
          Google Analytics by clicking on this
          link: http://tools.google.com/dlpage/gaoptout
          <br />
          <span className={styles.bulletStyle}>&bull;</span> Facebook: Set by
          Facebook only if you interacts with the Facebook plugin or is already
          logged into Facebook from other source for the purpose of connecting
          and integrating with them.
        </p>
        <p className={styles.subsubtitle}>Deletion of Cookies</p>
        <p className={styles.text}>
          You can delete the cookies already on your device. You can typically
          delete cookies from the Privacy or History area, available from the
          Settings or Options menu in the browser. In most browsers, the same
          menu can be reached through the Ctrl+Shift+Del keyboard shortcut or
          Command+Shift+Del if you are on a Mac.
        </p>
        <p className={styles.text}>
          If you do not accept Cookies from our website, you may experience
          inconvenience in your use of the website, and you may be prevented
          from accessing some of its features.
        </p>
        <p className={styles.subsubtitle}>Access your personal data</p>
        <p className={styles.text}>
          You can email inbox@greenpick.co and request information about your
          personal data.
        </p>
        <p className={styles.subsubtitle}>
          Correction and deletion of your personal data
        </p>
        <p className={styles.text}>
          If any of the personal data that we have about you in our capacity as
          a data controller is incorrect or misleading or if your personal data
          changes, you are welcome to e- mail inbox@greenpick.co and ask us to
          assist with correcting your information.
        </p>
        <p className={styles.text}>
          If you no longer wish to appear on the website, you can contact us
          at inbox@greenpick.co requesting to delete your personal data and
          reviews on the website.
        </p>
        <p className={styles.subsubtitle}>Other rights</p>
        <p className={styles.text}>
          In addition to the rights set out above concerning your personal data,
          you also have the following rights:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span> You also have the
          right to object to the processing of your personal data and have the
          processing of your personal data restricted.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> In particular, you
          have an unconditional right to object to the processing of your
          personal data for direct marketing purposes.
          <br />
          <span className={styles.bulletStyle}>&bull;</span> If our processing
          of your personal data is based on your consent, you have the right to
          withdraw your consent at any time. You may withdraw your consent by
          emailing us at inbox@greenpick.co.
        </p>
        <p className={styles.subsubtitle}>Children’s information</p>
        <p className={styles.text}>
          Our website is not directed to children under the age of 16, and we do
          not knowingly collect personal data from such children. If you become
          aware that a child under the age of 16 has provided us with their
          personal data, please contact us using the contact information
          provided below.
        </p>
        <p className={styles.subsubtitle}>Changes to this Policy</p>
        <p className={styles.text}>
          We reserve the right to make changes to this Policy. The date shown at
          the start of this Policy indicates when it was last revised. If we
          make material changes to it, we will provide notice through our
          website, or by other means, to give you the opportunity to review the
          changes before they come into effect. Your continued use of our
          website after we publish or send a notice about the changes to the
          Policy will mean that you accept and agree to the updated Policy.
        </p>
        <p className={styles.subsubtitle}>Contact information</p>
        <p className={styles.text}>
          If you have questions or concerns about our Policy, about the data
          processing activities performed by us, or would like to exercise your
          rights under our Policy, feel free to contact us
          at: inbox@greenpick.co.
        </p>
        <p className={styles.subtitle} id="termsAndConditions">
          Terms and Conditions
        </p>
        <p className={styles.text}>
          <i className={styles.italic}>December 2020</i>
        </p>
        <p className={styles.text}>
          These user terms and conditions (hereinafter referred to as the
          &quot;User Terms&quot;) apply to any use of Greenpick&#39;s website
          (hereinafter referred to as the &quot;website&quot;), including - but
          not limited to - <a href="www.greenpick.co">www.greenpick.co</a>
        </p>
        <p className={styles.text}>
          These User Terms are deemed to include all other operating rules,
          policies, and guidelines that are referred to herein or that we may
          otherwise publish on the website (as such, rules, policies and
          guidelines may be amended from time to time), including without
          limitation:
        </p>
        <p className={styles.text}>
          <span className={styles.bulletStyle}>&bull;</span>
          Our Guidelines, which are located at{" "}
          <a
            href="https://greenpick.co/terms&conditions/#guidelines"
            className={styles.text}
            style={{ fontFamily: "sans-serif" }}
          >
            <u>https://greenpick.co/terms&conditions/#guidelines</u>
          </a>
          .
          <br />
          <span className={styles.bulletStyle}>&bull;</span>Our Privacy Policy,
          which is located at{" "}
          <a
            href=" https://greenpick.co/terms&conditions/#privacyPolicy"
            className={styles.text}
            style={{ fontFamily: "sans-serif" }}
          >
            <u>https://greenpick.co/terms&conditions/#privacyPolicy</u>
          </a>
          .
        </p>
        <p className={styles.text}>
          By using the website, you accept to be subject to the User Terms,
          including the Guidelines. If you do not accept these User Terms, you
          are not permitted to use the website and are kindly requested not to
          use the website any further.
        </p>
        <p className={styles.subsubtitle}>General Terms</p>
        <ol type="1" className={styles.text + " " + styles.numberedList}>
          <li className={styles.orderedElement}>
            Rights
            <ol className={styles.numberedList} type="1">
              <li className={styles.orderedElement}>
                The website and the services we offer via the website, including
                all underlying technology and intellectual property rights
                embodied therein, are and remain our sole and exclusive
                property, and no license or any other right is granted to any
                such underlying technology. If you provide feedback, ideas or
                suggestions regarding the website or the services offered on the
                website (&quot;Feedback&quot;), we are free to fully exploit
                such Feedback.
              </li>
              <li className={styles.orderedElement}>
                The content on the website, including but not limited to the
                intellectual property rights, text, characteristics, graphics,
                icons, photos, calculations, references and software is or will
                be our property.
              </li>
              <li className={styles.orderedElement}>
                Downloading and other digital copying of the content on the
                website or parts hereof are only permitted for personal
                non-commercial use unless otherwise agreed with us in writing or
                allowed under applicable mandatory law.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Personal Data
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                We perform different types of processing of personal data in
                connection with the use of the website. Our processing of
                personal data takes place under observance of our Privacy
                Policy, which can be obtained{" "}
                <a href="/terms&conditions/#privacyPolicy">here</a>. By
                accepting these User Terms, you confirm to have read and
                accepted our Privacy Policy.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Disclaimers
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                THE WEBSITE, CONTENT AND SERVICES OFFERED ON THE WEBSITE ARE
                PROVIDED &#39;AS IS&#39; AND AS AVAILABLE WITHOUT
                REPRESENTATIONS OR WARRANTIES OF ANY KIND. GREENPICK EXPRESSLY
                DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS, IMPLIED OR STATUTORY,
                INCLUDING WITHOUT LIMITATION ANY WARRANTIES OF NON-INFRINGEMENT,
                MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. THE WEBSITE
                AND SERVICES MAY BE MODIFIED, UPDATED, INTERRUPTED, SUSPENDED OR
                DISCONTINUED AT ANY TIME WITHOUT NOTICE OR LIABILITY.
              </li>
              <li className={styles.orderedElement}>
                We make no representations or warranties with respect to any
                user generated content published on the website. Notwithstanding
                the foregoing, Greenpick may at all times investigate and edit
                (including anonymizing) user generated content, e.g. if such
                actions are (i) prompted by third party requests, (ii) required
                under applicable law or (iii) necessary for the user generated
                content&#39;s compliance with our User Guidelines.
              </li>
              <li className={styles.orderedElement}>
                We disclaim all liability for the content of user generated
                content. Our non- liability applies, without limitation, to any
                user generated content, including user generated content which
                has been edited by us (see 3.2). We are not liable for any links
                to third party websites in the user generated content, including
                for the content of the page to which the user generated content
                links.
              </li>
              <li className={styles.orderedElement}>
                Recommendations, reviews, comments, etc. of specific companies,
                services, e-businesses, etc. on the website are provided by
                users and are not endorsements made by us. We disclaim all
                liability for the content of the website. The use of our
                services is in any respect the sole responsibility of the users.
                We cannot be held liable for the availability of the website.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Other User Terms
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                We may at any time, in our sole discretion, revise or change
                these User Terms. We will provide reasonable advance notice of
                any such changes. We may at any time, in our own discretion and
                without notice, close, change or reorganize the website. As a
                user you accept to be covered by the at all times current User
                Terms. Any revision or change of the User Terms will be stated
                on the website. We will furthermore strive to inform the users
                about the change of the User Terms. The users agree that the
                continued use of the website after any posted modified version
                of the User Terms is an acceptance of the modified User Terms.
              </li>
              <li className={styles.orderedElement}>
                Should any of these User Terms be regarded as unlawful or
                without effect and therefore not to be enforced, this will not
                have any effect on the applicability and enforcement of the
                remaining part of the User Terms.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Term and termination
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                We may terminate your right to access and use the services
                offered on the website at any time for any reason without
                liability.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Copyright dispute policy
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                It is Greenpick&#39;s policy to (i) block access to or remove
                material that it believes in good faith to be copyrighted
                material that has been illegally copied and distributed by any
                of our advertisers, affiliates, content providers or members or
                users; and (ii) remove and discontinue service to repeat
                offenders.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Miscellaneous
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                You acknowledge and agree that these User Terms constitute the
                complete and exclusive agreement between you and Greenpick
                concerning your use of the website and supersede and govern all
                prior proposals, agreements or other communications.
              </li>
              <li className={styles.orderedElement}>
                Nothing contained in these User Terms can be construed as
                creating any agency, partnership or other form of joint
                enterprise between us. Our failure to require your performance
                of any provision hereof will not affect our full right to
                require such performance at any time thereafter, nor may our
                waiver of a breach of any provision hereof be taken or held to
                be a waiver of the provision itself. You may not assign any
                rights granted to you hereunder, and any such attempts are void.
              </li>
            </ol>
          </li>
          <li className={styles.orderedElement}>
            Contact
            <ol className={styles.numberedList}>
              <li className={styles.orderedElement}>
                You may contact Greenpick via e-mail at inbox@greenpick.co.
              </li>
            </ol>
          </li>
        </ol>
      </div>
    </div>
  );
};

export default TermsAndConditionsPage;
