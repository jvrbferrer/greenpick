import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import Template from "./Template";
import CompaniesEmailReview from "./CompaniesEmailReview";
// import Button from "./EmailButton";

const CompaniesEmail = ({ newWindow, name, reviews }) => {
  const [container] = useState(document.createElement("div"));

  useEffect(() => {
    newWindow.document.body.innerHTML = "";

    newWindow.document.body.appendChild(container);
  }, [newWindow, container]);

  return ReactDOM.createPortal(
    <Template>
      <tr>
        <td
          style={{
            padding: "0px 24px 24px",
            fontSize: "28px",
            lineHeight: "32px",
            color: "#53594F",
          }}
        >
          <p>
            {name} has received {reviews.length} new eco-review
          </p>
        </td>
      </tr>
      <tr>
        <td
          style={{
            padding: "0px 24px 24px",
            fontSize: "16px",
            lineHeight: "24px",
            color: "#737373",
          }}
        >
          <p>
            Greenpick is developing an eco-conscious reviewing app to empower
            consumers to rate, share &amp; discover the sustainability of
            products &amp; brands.
          </p>
          <p>
            On Greenpicks's first initiative near consumers, {name} has reveived{" "}
            {reviews.length} eco-review
            {reviews.length > 1 ? "s" : ""}
          </p>
        </td>
      </tr>
      {/* <tr>
        <td
          style={{
            padding: "0px 24px 24px",
          }}
        >
          <Button width="168px" link="">
            Read reviews online (under constructions)
          </Button>
        </td>
      </tr> */}
      {reviews.map((review) => {
        return (
          <React.Fragment key={review.id}>
            <tr>
              <td
                style={{
                  padding: "0px 24px 24px",
                }}
              >
                <CompaniesEmailReview review={review} />
              </td>
            </tr>
            {review.comments.map((comment) => {
              return (
                <tr key={comment.id + 1233}>
                  <td
                    style={{
                      padding: "0px 24px 24px 72px",
                    }}
                  >
                    <CompaniesEmailReview
                      review={comment}
                      backgroundColor="#F2F2F2"
                    />
                  </td>
                </tr>
              );
            })}
          </React.Fragment>
        );
      })}
      <tr>
        <td
          style={{
            padding: "0px 24px 24px",
            fontSize: "16px",
            lineHeight: "24px",
            color: "#737373",
          }}
        >
          <p>
            Consumers are paying attention to the sustainability of {name}’s
            products! Greenpick urges {name} to join this movement and provide
            feedback to consumers’ eco-reviews.
          </p>
          <p>
            Find out more about our initiative at{" "}
            <a href="http://greenpick.co">greenpick.co</a>.
          </p>
          <p>
            Consumers on Greenpick are looking forward to hearing from {name}.
          </p>
        </td>
      </tr>
      <tr>
        <td
          style={{
            padding: "0px 24px 24px",
            fontSize: "16px",
            lineHeight: "24px",
            color: "#737373",
          }}
        >
          <p style={{ marginBottom: "5px" }}>Best regards,</p>
          <p style={{ marginTop: "0px" }}>Greenpick Team</p>
        </td>
      </tr>
      {/* <tr>
        <td
          style={{
            padding: "0px 24px 24px",
          }}
        >
          <Button width="350px" link="">
            Read reviews online (under construction)
          </Button>
        </td>
      </tr> */}
    </Template>,
    container
  );
};

export default CompaniesEmail;
