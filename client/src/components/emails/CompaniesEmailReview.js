import React from "react";

const CompananiesEmailReview = ({ review, backgroundColor = "#F4F6F3" }) => {
  return (
    <table
      style={{
        padding: "20px 20px 16px",
        backgroundColor: backgroundColor,
        width: "100%",
      }}
    >
      <tbody>
        <tr>
          <td
            style={{
              height: "32px",
              width: "32px",
            }}
          >
            <img
              src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/defaultUserImage.svg"
              alt="person pic"
              style={{
                height: "32px",
                width: "32px",
              }}
            />
          </td>
          <td
            style={{
              paddingLeft: "15px",
              fontSize: "14px",
              fontWeight: "600",
              lineHeight: "16px",
            }}
          >
            <p
              style={{
                fontSize: "14px",
                fontWeight: "600",
                lineHeight: "16px",
                margin: "0px",
              }}
            >
              {review.user.name || "Anonymous"}
            </p>
            <p
              style={{
                fontSize: "12px",
                color: "#3A3F37",
                margin: "0px",
                lineHeight: "19px",
                fontWeight: "400",
              }}
            >
              {review.createdAt.split("T")[0]}
            </p>
          </td>
        </tr>
        {review.originalLang !== "en" ? (
          <>
            <tr>
              <td
                style={{
                  fontSize: "14px",
                  lineHeight: "19px",
                  padding: "16px 0 0",
                }}
                colSpan="2"
              >
                <p>(Translated by Amazon) {review.textEn}</p>
              </td>
            </tr>
            <tr>
              <td
                style={{
                  fontSize: "12px",
                  lineHeight: "17px",
                  padding: "16px 0 0",
                  color: "#828282",
                }}
                colSpan="2"
              >
                <p>(Original) {review.text}</p>
              </td>
            </tr>
          </>
        ) : (
          <tr>
            <td
              style={{
                fontSize: "14px",
                lineHeight: "14px",
                padding: "16px 0 0",
              }}
              colSpan="2"
            >
              <p>{review.text}</p>
            </td>
          </tr>
        )}
        {review.images && review.images.length > 0 && (
          <tr>
            <td
              style={{
                fontSize: "12px",
                lineHeight: "17px",
                padding: "16px 0 0",
                color: "#828282",
              }}
              colSpan="2"
            >
              <img
                src={review.images[0]}
                alt=""
                style={{
                  width: "200px",
                  maxWidth: "100%",
                  height: "auto",
                }}
              />
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export default CompananiesEmailReview;
