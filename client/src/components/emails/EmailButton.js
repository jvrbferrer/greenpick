import React from "react";

const ButtonEmail = ({ children, width, link }) => {
  return (
    <a
      href={link}
      style={{
        fontSize: "16px",
        fontWeight: "bold",
        lineHeight: "50px",
        textAlign: "center",
        textDecoration: "none",
      }}
    >
      <table>
        <tbody>
          <tr>
            <td
              style={{
                backgroundColor: "#93A086",
                color: "#ffffff",
                padding: "12px 18px",
              }}
            >
              {children}
            </td>
          </tr>
        </tbody>
      </table>
    </a>
  );
};

export default ButtonEmail;
