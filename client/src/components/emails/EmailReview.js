import React from "react";

const EmailReview = () => {
  return (
    <table style={{ width: "100%" }}>
      <tr>
        <td
          style={{
            padding: "10px 24px 10px 24px",
          }}
        >
          <img
            src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/logo%403x.png"
            alt="Greenpick"
            width="120"
            style={{ display: "block" }}
          />
        </td>
      </tr>
    </table>
  );
};

export default EmailReview;
