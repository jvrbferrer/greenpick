import React from "react";

const Template = ({ children }) => {
  return (
    <table
      style={{
        width: "100%",
        backgroundColor: "#efefef",
      }}
    >
      <tbody>
        <tr>
          <td>
            <table
              align="center"
              cellPadding="0"
              cellSpacing="0"
              width="600"
              style={{
                borderCollapse: "collapse",
                backgroundColor: "white",
                fontFamily: "Arial, sans-serif",
              }}
            >
              <tbody>
                <tr>
                  <td
                    style={{
                      padding: "34px 0px 18px 24px",
                    }}
                  >
                    <img
                      src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/Group%403x.png"
                      alt="Greenpick"
                      width="120"
                      style={{
                        display: "block",
                      }}
                    />
                  </td>
                </tr>
                {children}
                <tr>
                  <td
                    style={{
                      padding: "10px 24px 10px 24px",
                    }}
                  >
                    <hr
                      style={{
                        backgroundColor: "#666",
                      }}
                    />
                  </td>
                </tr>
                <tr>
                  <td
                    style={{
                      padding: "10px 24px 10px 24px",
                    }}
                  >
                    <img
                      src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/logo%403x.png"
                      alt="Greenpick"
                      width="120"
                      style={{ display: "inline" }}
                    />
                  </td>
                </tr>
                <tr>
                  <td
                    style={{
                      padding: "10px 24px 10px 24px",
                    }}
                  >
                    <a href="https://www.facebook.com/greenpick.hq/">
                      <img
                        src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/SocialThumbnails/FBThumbnail.jpg"
                        alt="Facebook"
                        style={{
                          display: "inline",
                          width: "25px",
                          height: "25px",
                        }}
                      />
                    </a>
                    <a href="https://www.instagram.com/greenpick.co/">
                      <img
                        src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/SocialThumbnails/InstaThumbnail.jpg"
                        alt="Instagram"
                        style={{
                          display: "inline",
                          width: "25px",
                          height: "25px",
                          marginLeft: "8px",
                        }}
                      />
                    </a>
                    <a href="https://www.reddit.com/r/Greenpick/">
                      <img
                        src="https://elasticbeanstalk-eu-central-1-228390497504.s3.eu-central-1.amazonaws.com/resources/images/SocialThumbnails/RedditThumbnail.jpg"
                        alt="Greenpick"
                        style={{
                          display: "inline",
                          width: "25px",
                          height: "25px",
                          marginLeft: "8px",
                        }}
                      />
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default Template;
