import { authenticate as authenticateAPI } from "../utils/APICalls";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import jwtDecode from "jwt-decode";

export const authenticate = createAsyncThunk(
  "authentication/authenticate",
  async (params, thunkAPI) => {
    try {
      let res = await authenticateAPI({
        auth: { email: params.email, password: params.password }
      });
      if (res._ok) {
        return Promise.resolve(res.jwt);
      }
      return Promise.reject(res);
    } catch (error) {
      return Promise.reject(error);
    }
  }
);

const authenticationSlice = createSlice({
  name: "authentication",
  initialState: {
    loading: false,
    authenticated: {
      value: false,
      admin: false
    }
  },
  reducers: {
    loadState: (state, action) => {
      const token = window.sessionStorage.getItem("token");
      const tokenExpirationTime = new Date(
        window.sessionStorage.getItem("tokenExpirationTime")
      );
      const tokenAdmin = window.sessionStorage.getItem("tokenAdmin");
      if (
        token &&
        tokenExpirationTime &&
        tokenExpirationTime > Date.now() &&
        tokenAdmin
      ) {
        state.authenticated.value = true;
        state.authenticated.admin = JSON.parse(tokenAdmin);
      } else {
        state.authenticated.value = false;
        state.authenticated.admin = false;
      }
    },
    logout: (state, action) => {
      state.authenticated.value = false;
      state.authenticated.admin = false;
      window.sessionStorage.removeItem("token");
      window.sessionStorage.removeItem("tokenExpirationTime");
    }
  },
  extraReducers: {
    [authenticate.pending]: (state, action) => {
      state.loading = true;
    },
    [authenticate.fulfilled]: (state, action) => {
      state.loading = false;
      state.authenticated.value = true;
      let decodedToken = jwtDecode(action.payload);
      state.authenticated.admin = decodedToken.admin;

      let expirationDate = new Date(decodedToken.exp * 1000);
      window.sessionStorage.setItem("token", action.payload);
      window.sessionStorage.setItem("tokenExpirationTime", expirationDate);
      window.sessionStorage.setItem("tokenAdmin", decodedToken.admin);
    },
    [authenticate.rejected]: (state, action) => {
      state.loading = false;
      state.authenticated.value = false;
      state.authenticated.admin = false;
    }
  }
});

export default authenticationSlice.reducer;

export const { logout, loadState } = authenticationSlice.actions;
