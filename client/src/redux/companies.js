import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";

const companiesAdapter = createEntityAdapter({
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

const fetchCompanies = createAsyncThunk("companies/fetchAllSince", async () => {
  //Make API call to get companies since last fetch and await
  const response = {
    companies: [
      { name: "Colgate", id: 123 },
      { name: "El primo sauce", id: 1244 },
      { name: "Dragons for sale", id: 77177 },
    ],
    lastFetch: 1500,
  };
  return response;
});

const createCompany = createAsyncThunk(
  "companies/createCompany",
  async (name) => {
    //Make API call to create company and await
    const response = { _ok: true, name, id: 3000 };
    return response;
  }
);

const companiesSlice = createSlice({
  name: "companies",
  initialState: companiesAdapter.getInitialState({
    loading: true,
    lastFetch: 0,
    loadingId: null,
    loadingSubmit: false,
  }),
  reducers: {},
  extraReducers: {
    [createCompany.pending]: (state, action) => {
      state.loadingSubmit = true;
    },
    [createCompany.fulfilled]: (state, action) => {
      companiesAdapter.addOne(state, {
        name: action.payload.name,
        id: action.payload.id,
      });
      state.loadingSubmit = false;
      state.lastFetch = action.payload.lastFetch;
    },
    [createCompany.rejected]: (state, action) => {
      state.loadingSubmit = false;
    },
    [fetchCompanies.pending]: (state, action) => {
      state.loading = true;
    },
    [fetchCompanies.fulfilled]: (state, action) => {
      companiesAdapter.upsertMany(state, action.payload.companies);
      state.loading = false;
      state.lastFetch = action.payload.lastFetch;
    },
    [fetchCompanies.rejected]: (state, action) => {
      state.loading = false;
    },
  },
});

export const {
  selectById: selectCompanyById,
  selectIds: selectCompanyIds,
  selectEntities: selectCompanyEntities,
  selectAll: selectAllCompanies,
  selectTotal: selectTotalCompanies,
} = companiesAdapter.getSelectors((state) => state.companies);
export { fetchCompanies, createCompany }; //Export thunks
export default companiesSlice.reducer;
