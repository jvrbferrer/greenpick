import {
  createSlice,
  createAsyncThunk,
  createSelector,
  createEntityAdapter,
} from "@reduxjs/toolkit";

const reviewsAdapter = createEntityAdapter();

const fetchCompanyReviews = createAsyncThunk(
  "companies/fetchInfoAndReviews",
  async (companyId) => {
    const response = {
      reviews: [
        {
          id: 123,
          companyId: 123,
          description: "Suckss",
          images: [
            "https://www.dreamgrow.com/wp-content/uploads/2011/11/google-chrome.jpg",
            "https://www.dreamgrow.com/wp-content/uploads/2011/11/allstate.jpg",
          ],
          userId: 321,
        },
        {
          id: 12345,
          companyId: 123,
          description: "Sucks very muchhhh",
          images: [
            "https://www.dreamgrow.com/wp-content/uploads/2011/11/pepsi.jpg",
          ],
          userId: 999,
        },
      ],
    };
    return { reviews: response.reviews, id: companyId };
  }
);

const reviewsSlice = createSlice({
  name: "reviews",
  initialState: reviewsAdapter.getInitialState({
    loadingId: null,
  }),
  reducers: {},
  extraReducers: {
    [fetchCompanyReviews.pendind]: (state, action) => {
      state.loadingId = action.payload.id;
    },
    [fetchCompanyReviews.fulfilled]: (state, action) => {
      reviewsAdapter.upsertMany(state, action.payload.reviews);
      state.loadingId = null;
    },
    [fetchCompanyReviews.rejected]: (state, action) => {
      state.loadingId = null;
    },
  },
});

const selectReviewsByCompany = (companyId) => {
  return (state) => {
    return Object.values(state.reviews.entities).filter(
      (item) => item.companyId === companyId
    );
  };
};

export { selectReviewsByCompany }; //Export selectors
export { fetchCompanyReviews }; //Export thunks
export default reviewsSlice.reducer;
