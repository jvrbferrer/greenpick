import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import authenticationReducer from "./authentication";
import companiesReducer from "./companies";

const store = configureStore({
  reducer: combineReducers({
    authentication: authenticationReducer,
    companies: companiesReducer
  })
});

window.store = store;

export default store;
