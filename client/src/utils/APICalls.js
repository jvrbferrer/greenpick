/*
If an API method calls jsonify then 
besides the response json fields being present, 
_ok means wether the request was successful.
*/
const jsonify = response => {
  let ok = response.ok;
  let status = response.status;
  return new Promise((accept, reject) => {
    response
      .json()
      .then(response => {
        response._ok = ok;
        response._status = status;
        accept(response);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const addAuthorization = headers => {
  const token = window.sessionStorage.getItem("token");
  if (token) {
    headers.push(["Authorization", "Bearer " + token]);
  }
};

export const getGP = (url, params = {}) => {
  url = new URL(`/api/${url}`, window.location.origin);
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  let headers = [["Accept", "application/json"]];
  addAuthorization(headers);
  return fetch(url, {
    headers,
    method: "GET",
    mode: "cors"
  });
};

export const postGP = (url, data) => {
  url = new URL(`/api/${url}`, window.location.origin);
  let headers = [
    ["Content-Type", "application/json"],
    ["Accept", "application/json"]
  ];
  addAuthorization(headers);
  return fetch(url, {
    headers,
    method: "POST",
    mode: "cors",
    body: JSON.stringify(data)
  });
};

export const presignedPostAWS = async (presignedPost, image) => {
  const formData = new FormData();
  for (const name in presignedPost.fields) {
    formData.append(name, presignedPost.fields[name]);
  }
  formData.append("file", image);
  const options = {
    body: formData,
    method: "POST",
    mode: "cors"
  };
  return await fetch(presignedPost.url, options);
};

/*
data: {
  email: optional
  phone: optional
  name: optional
  sendEmail: required
  password: required
  signedUp: required. boolean
}
Either email or phone number must be set
returns {
  user
}
*/
export const createUser = data => {
  return postGP("users", data).then(response => jsonify(response));
};

/*
data: {
  email: optional
  phone: optional
  name: optional
  password: required
  signedUp: required. boolean
}
Either email or phone number must be set
returns {
  user
}
*/
export const updateUser = data => {
  return postGP("users/update", data).then(response => jsonify(response));
};

/*
data: {
  email: <email>
  name: <name>
  code_string: <code to be submitted>
}
on success:
  returns: {prize: type_of_prize}
*/
export const submitCode = data =>
  postGP("prize_codes/submit", data).then(response => jsonify(response));

/*
returns: {tree_count: <number of trees planted>}
*/
export const getNTreesPlanted = () =>
  getGP("prize_codes/trees/count").then(response => jsonify(response));

/*
returns: {count: <number of reviews made>}
*/
export const getNReviews = () =>
  getGP("reviews/count").then(response => jsonify(response));

/*
data: {
  info: {
    email: <email>,
    name: <name>,
    text: <message>
  }
}
*/
export const receiveContact = data =>
  postGP("users/contact", data).then(response => jsonify(response));

/*
data: {
  codeString: <code>
}
*/
export const getCodeInfo = data =>
  getGP("prize_codes/info", data).then(response => jsonify(response));

/*
data: {
  userId
  companyId: companyId
  title: <optional>
  text: review text
  images: images to upload
  createdAt: optional
}
return: {
  postUrls: urls to post the images
  getUrls: urls to get the images posted
}
*/
export const createReview = async data => {
  data.filenames = data.images.map(image => image.name);
  data.fileTypes = data.images.map(image => image.type);
  const response = await jsonify(await postGP("reviews", data));

  for (let i = 0; i < data.images.length; i++) {
    await presignedPostAWS(response.postUrls[i], data.images[i]);
  }
  return response;
};

/*
data: {
  reviewId
}
return: {
}
*/
export const deleteReview = async data => {
  return await jsonify(await postGP("reviews/delete", data));
};

/*
data: {
  begName: string to search as the beginning of the name of the company
}
*/
export const searchCompany = data =>
  getGP("companies/search_name", data).then(options => jsonify(options));

/*
data: {
  name
  countryCode
  verified
  logo //optional
  website //optional
}
returns : companyCreated
*/
export const createCompany = async data => {
  if (data.logo) {
    data.logoFilename = data.logo.name;
  }
  const response = await jsonify(await postGP("companies", data));
  if (response._ok && data.logo) {
    presignedPostAWS(response.presignedPost, data.logo);
    // let awsRes = await presignedPostAWS(response.presignedPost, data.logo);
    // awsRes = await awsRes.text();
  }
  return response;
};

/*
data: {
  id
  name //optional
  countryCode //optional
  verified //optional
  logo //optional
  website //optional
}
return updatedCompany
*/
export const updateCompany = async data => {
  if (!!data.logo) {
    data.logoFilename = data.logo.name;
    data.logoFileType = data.logo.type;
  }
  const response = await jsonify(await postGP("companies/update", data));
  if (response._ok && !!data.logo) {
    presignedPostAWS(response.presignedPost, data.logo);
  }
  return response;
};

/*
data: {
  companyId: company ID to search
}
*/
export const findCompany = data =>
  getGP("companies/find", data).then(company => jsonify(company));

/*
data: {
  companyId: company ID to whom the reviews belong to 
}
returns: {
  reviews: reviews of the company
}
*/
export const getCompanyReviews = data =>
  getGP("companies/reviews", data).then(result => jsonify(result));

/*
data: {
  companyId: company ID to whom the reviews belong to 
}
returns: {}
*/
export const deleteCompany = data => {
  return postGP("companies/delete", data).then(result => jsonify(result));
};

/*
data {
  query: query to search in user name, phone number or email
}
return: {
  users: users that match that query
}
*/
export const findUsersByAllFields = data =>
  getGP("users/search", data).then(result => jsonify(result));

/*
data {
  email: required
}
return: {
}
*/
export const addToMailingList = data => {
  return postGP("users/add_to_mailing_list", data).then(result =>
    jsonify(result)
  );
};

/*
data {
  auth {
    email: required
    password: required
  }
}
return {
  jwt
}
*/
export const authenticate = data => {
  return postGP("user_token", data).then(result => jsonify(result));
};

/*
data {}
return {
  prizeCode
}
*/
export const createCode = () => {
  return postGP("prize_codes", {}).then(result => jsonify(result));
};

/*
data {}
return {
  prizeCodes
}
*/
export const getCodes = () => {
  return getGP("prize_codes").then(result => jsonify(result));
};

/*
data {
  userID
  reviewId
  text
  createdAt
}
*/
export const createComment = data => {
  return postGP("comments", data).then(result => jsonify(result));
};
