import { useLayoutEffect, useEffect } from "react";
import { useHistory } from "react-router-dom";

const useChangeNavbarColor = setHeaderColor => {
  useLayoutEffect(() => {
    setHeaderColor();
  }, [setHeaderColor]);
};

const useTracking = (trackingId = "UA-172763903-1") => {
  const { listen } = useHistory();
  useEffect(() => {
    const unlisten = listen(location => {
      if (!window.gtag) return;
      if (!trackingId) {
        console.log("Tracking not enabled, as `trackingId` was not given.");
        return;
      }
      window.gtag("config", trackingId, {
        page_path: location.pathname,
        send_page_view: process.env.NODE_ENV === "production"
      });
    });
    return unlisten;
  }, [trackingId, listen]);
};

export { useChangeNavbarColor, useTracking };
