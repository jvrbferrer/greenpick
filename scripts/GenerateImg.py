import os
import sys
from PIL import Image
import argparse

initial_directory = "../client/src/assets/img/"

all_folders = list(
    map(
        lambda x: os.path.join(initial_directory, x),
        [
            "AboutBanner",
            "Carousel/Image1",
            "Carousel/Image2",
            "Carousel/Image3",
            "Carousel/Image4",
            "Carousel/Image5",
            "Carousel/Comment1_1",
            "Carousel/Comment1_2",
            "Carousel/Comment2_1",
            "Carousel/Comment3_1",
            "Carousel/Comment3_2",
            "Carousel/Comment4_1",
            "Carousel/Comment5_1",
            "CicleOfGreenpick",
            "Forest",
            "Logo",
            "Soil",
            "Supports/IAPMEI",
            "Supports/Lisboa2020",
            "Supports/Portugal2020",
            "Supports/UE",
            "Team/AfonsoPinheiro",
            "Team/BeatrizFerrer",
            "Team/CarolinaRaposo",
            "Team/DiogoFerrer",
            "Team/HenriqueFerrer",
            "Team/InesDuverge",
            "Team/JoaquimFerrer",
            "Team/RicardoFreitas",
            "Team/RitaSantos",
            "Team/",
            "ReviewsPage",
            "App"
        ],
    )
)


parser = argparse.ArgumentParser()
parser.add_argument("-r", required=False, action="store_true")
parser.add_argument("-f", required=False)

args = parser.parse_args()
print(parser.parse_args)
quality = 90  # 90%

directories = []
if args.r:
    directories = all_folders
else:
    directories = [initial_directory + args.f]


for directory in directories:
    sizes = []
    true_extension = ""
    old_images = []
    new_images = []
    default_img = ""
    index_file = open(os.path.join(directory, "index.js"), "w")
    min_width = float("inf")
    for base in os.listdir(directory):
        filename = os.path.join(directory, base)
        base_without_extension, extension = os.path.splitext(base)
        nice_name = base_without_extension.replace("@", "")
        nice_name = nice_name.replace("-", "")
        nice_name = nice_name.replace(" ", "")

        filename_without_extension = os.path.splitext(filename)[0]
        if extension == ".png" or extension == ".jpeg" or extension == ".jpg":
            true_extension = extension
            os.system(
                "cwebp -q %d %s -o %s.webp"
                % (quality, filename, filename_without_extension)
            )
            index_file.write('import %s from "./%s";\n' % (nice_name, base))
            index_file.write(
                'import %s_new from "./%s.webp";\n'
                % (nice_name, base_without_extension)
            )
            old_images.append(nice_name)
            new_images.append(nice_name + "_new")
            with Image.open(filename) as img:
                width, _ = img.size
                if width < min_width:
                    default_img = base_without_extension
                sizes.append(width)

    index_file.write(
        """
export default {
  oldImages: [%s],
  newImages: [%s],
  sizes: [%s],
  format: "%s",
  defaultImg: "%s"
}"""
        % (
            ", ".join(old_images),
            ", ".join(new_images),
            ", ".join(list(map(str, sizes))),
            true_extension[1:],
            default_img,
        )
    )
