docker build -t greenpick/test-client ./client
docker build -t greenpick/test-server ./server
docker build -t greenpick/test-nginx ./nginx

docker push greenpick/test-client
docker push greenpick/test-server
docker push greenpick/test-nginx