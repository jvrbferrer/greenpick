class ApplicationController < ActionController::API
  include Knock::Authenticable

  def admin?
    render status: :unauthorized unless current_user&.is_admin
  end
end
