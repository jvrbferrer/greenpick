# Endpoints for prize code management
class CodesController < ApplicationController
  before_action :admin?, only: %i[create index]

  def create
    code = nil

    until code
      code_string = "GP-#{SecureRandom.alphanumeric(7).upcase}"
      code = PrizeCode.create(
        code_string: code_string,
        prize: params[:prize].to_i
      )
    end

    render json: { prizeCode: code }, status: :ok
  end

  def tree_count
    trees = PrizeCode.where(prize: :tree, used: true)

    render json: { treeCount: trees.count }, status: :ok
  end

  def submit
    prize_code = PrizeCode.find_by(code_string: params[:codeString])

    if prize_code
      if prize_code.used
        render json: {
          error: { codeString: 'Prize code was already claimed.' }
        }, status: :not_acceptable
      else
        update_user_prize(prize_code)

        render json: {
          prize: prize_code.prize
        }, status: :ok
      end
    else
      render json: {
        error: { codeString: 'Invalid prize code.' }
      }, status: :not_acceptable
    end
  end

  def info_for_code
    prize_code = PrizeCode.find_by(code_string: params[:codeString])

    if prize_code
      if prize_code.user
        render json: {
          name: prize_code.user.name,
          prize: prize_code.prize
        }, status: :ok
      else
        render json: {
          error: { user: 'No user has claimed this prize.' }
        }, status: :not_acceptable
      end
    else
      render json: {
        error: { codeString: 'Invalid prize code.' }
      }, status: :not_acceptable
    end
  end

  def index
    prize_codes = PrizeCode.all.order('created_at DESC')
    render json: {
      prizeCodes: prize_codes
    }, status: :ok
  end

  private

  def update_user_prize(prize_code)
    user = User.find_by(email: params[:email])
    user ||= User.create(email: params[:email])

    user.update(name: params[:name])
    prize_code.update(user_id: user.id, used: true)
  end

end
