class CommentsController < ApplicationController
  include AwsTranslatorUtils
  before_action :admin?, only: %i[create]

  def create
    user_id = params[:userId]
    review_id = params[:reviewId]
    text = params[:text]
    created_at = DateTime.current
    created_at = params[:createdAt] if params[:createdAt] && params[:createdAt] != ''

    if !User.find(user_id)
      return render json: {
        error: {
          userId: 'User not found'
        }
      }, status: :unprocessable_entity
    end
    if !Review.find(review_id)
      return render json: {
        error: {
          reviewId: 'Review not found'
        }
      }, status: :unprocessable_entity
    end

    comment = Comment.new(
      user_id: user_id,
      review_id: review_id,
      text: text,
      created_at: created_at,
      updated_at: created_at
    )

    translated_text, source_language = translate_text_to_english(
      text
    ).values_at(
      :translated_text,
      :source_language
    )
    comment.original_lang = source_language
    comment.text_en = translated_text if source_language != 'en'

    if comment.save
      render json: {}, status: :ok
    else
      render json: {
        error: comment.errors.messages
      }, status: :unprocessable_entity
    end
  end
end
