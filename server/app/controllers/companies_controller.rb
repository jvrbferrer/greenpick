class CompaniesController < ApplicationController
  include AwsBucketUtils
  before_action :admin?, only: %i[create update delete search_name reviews]

  def create
    logo_filename = params[:logoFilename]
    country_code = params[:countryCode]
    name = params[:name]
    verified = params[:verified]
    website = params[:website]

    presigned_post = image_key = nil
    presigned_post, image_key = get_aws_image_urls(logo_filename, 'logos') if logo_filename
    company = Company.create(
      name: name,
      country_code: country_code,
      verified: verified,
      logo_key: image_key,
      website: website
    )
    if company.valid?
      render json: {
        presignedPost: presigned_post,
        getUrl: get_aws_image_url(image_key)
      }, status: :ok
    else
      render json: {
        error: company.errors.messages
      }, status: :unprocessable_entity
    end
  end

  def update
    company_id = params[:companyId]
    logo_filename = params[:logoFilename]
    country_code = params[:countryCode]
    name = params[:name]
    verified = params[:verified]
    website = params[:website]

    company = Company.find(company_id)
    if !company
      render json: {
        error: 'Company not found'
      }, status: :unprocessable_entity
    end
    image_key = company.logo_key
    if logo_filename
      delete_aws_image company.logo_key if company.logo_key
      presigned_post, image_key = get_aws_image_urls(logo_filename, 'logos')
    end

    success = company.update(
      {
        name: name,
        country_code: country_code,
        verified: verified,
        logo_key: image_key,
        website: website
      }
    )
    if success
      render json: {
        presignedPost: presigned_post,
        getUrl: get_aws_image_url(image_key)
      }, status: :ok
    else
      render json: {
        error: company.errors.messages
      }, status: :unprocessable_entity
    end
  end

  def delete
    company_id = params[:companyId]
    company = Company.find(company_id)
    if !company
      return render json: {
        error: 'Company not found'
      }, status: :unprocessable_entity
    end
    delete_aws_image(company.logo_key) if company.logo_key && company.logo_key != ''
    company.destroy
    return render json: {}, status: :ok
  end

  def search_name
    beg_name = params[:begName].downcase
    companies = Company.select(:name, :id).where('lower(name) LIKE ?', "%#{beg_name}%").order(:name)
    render json: {
      companies: companies
    }, status: :ok
    return
  end

  def find
    company_id = params[:companyId]
    company = Company.find(company_id)
    n_reviews = company.reviews.count
    if company
      render json: {
        id: company_id,
        name: company.name,
        countryCode: company.country_code,
        verified: company.verified,
        logoUrl: get_aws_image_url(company.logo_key),
        website: company.website,
        nReviews: n_reviews
      }, status: :ok
    else
      render json: {
        error: 'Company not found'
      }, status: :unprocessable_entity
    end
  end

  def reviews
    company_id = params[:companyId]
    company = Company.find(company_id)
    if !company
      return render json: {
        error: 'Company not found'
      }, status: :unprocessable_entity
    end

    reviews = Review.where(company_id: company_id).includes(
      :review_images, :user, { comments: :user }
    )

    reviews = reviews.map do |review|
      {
        "images": review.review_images.map { |review_image| get_aws_image_url(review_image.key) },
        "title": review.title,
        "text": review.text,
        "originalLang": review.original_lang,
        "titleEn": review.title_en,
        "textEn": review.text_en,
        "id": review.id,
        "user": review.user,
        "createdAt": review.created_at,
        "comments": review.comments.map do |comment|
          {
            "id": comment.id,
            "text": comment.text,
            "textEn": comment.text_en,
            "user": comment.user_public,
            "createdAt": comment.created_at,
            "originalLang": comment.original_lang
          }
        end
      }
    end

    return render json: { reviews: reviews }, status: :ok
  end
end
