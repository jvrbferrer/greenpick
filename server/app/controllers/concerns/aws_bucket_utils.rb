module AwsBucketUtils
  def get_aws_image_urls(filename, path, unique_identifier = '')
    unique_identifier = SecureRandom.uuid if unique_identifier == ''
    key = "#{path}/#{unique_identifier}/#{filename}".gsub(/\s+/, '')
    presigned_post = S3_BUCKET.presigned_post(
      {
        key: key,
        acl: 'public-read',
        content_type_starts_with: 'image/',
        metadata: {
          'original-filename' => '${filename}'
        }
      }
    )
    return presigned_post, key
  end

  def delete_aws_image(key)
    S3_BUCKET.object(key).delete
  end

  def get_aws_image_url(key)
    return nil if !key

    return URI.join(S3_BUCKET.url, key)
  end

end
