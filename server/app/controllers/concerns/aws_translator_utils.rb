module AwsTranslatorUtils
  def translate_text_to_english(text)
    response = AWS_TRANSLATE.translate_text(
      {
        text: text,
        source_language_code: 'auto',
        target_language_code: 'en'
      }
    )
    return {
      translated_text: response.translated_text,
      source_language: response.source_language_code
    }
  end
end
