class ReviewsController < ApplicationController
  include AwsBucketUtils
  include AwsTranslatorUtils
  before_action :admin?, only: %i[create delete]

  def create
    user_id = params[:userId]
    company_id = params[:companyId]
    filenames = params[:filenames]
    file_types = params[:fileTypes]
    text = params[:text]
    created_at = DateTime.current
    created_at = params[:createdAt] if params[:createdAt] && params[:createdAt] != ''
    # Removing all \n to create a separation between title and text
    # during translation with \n
    title = params[:title].tr("\n", ' ')

    if !User.find(user_id)
      return render json: {
        error: {
          userId: 'User not found'
        }
      }, status: :unprocessable_entity
    end

    if !Company.find(company_id)
      return render json: {
        error: {
          companyId: 'Company not found'
        }
      }, status: :unprocessable_entity
    end

    # Check max number of images
    if filenames.length > 3 || filenames.length != file_types.length
      render json: {
        error: 'Error processing images'
      }, status: :unprocessable_entity
      return
    end

    review = Review.new(
      text: text,
      company_id: company_id,
      user_id: user_id,
      created_at: created_at,
      updated_at: created_at
    )
    post_urls = Array.new(filenames.length)
    get_urls = Array.new(filenames.length)
    (0..filenames.length - 1).each do |i|
      post_url, image_key = get_aws_image_urls(filenames[i], 'reviewImages')
      post_urls[i] = post_url
      get_urls[i] = get_aws_image_url(image_key)
      review.review_images.build(key: image_key)
    end
    review.title = title if title
    if text.length < 2000
      translated_title, translated_text, source_language = translate_text_title(
        text,
        title
      ).values_at(:translated_title, :translated_text, :source_language)
      review.original_lang = source_language
      if source_language != 'en'
        review.text_en = translated_text
        review.title_en = translated_title
      end
    end
    success = review.save
    if success
      render json: {
        postUrls: post_urls,
        getUrls: get_urls
      }, status: :ok
    else
      render json: {
        error: review.errors.messages
      }, status: :unprocessable_entity
    end
  end

  def delete
    review_id = params['reviewId']

    review = Review.includes(:review_images).find(review_id)
    if !review
      return render json: {
        error: 'Review not found'
      }, status: :unprocessable_entity
    end
    review.review_images.each do |review_image|
      delete_aws_image(review_image.key)
      review_image.destroy
    end
    review.comments.destroy_all
    review.destroy
    return render json: {}, status: :ok
  end

  def count
    count = Review.all.count
    render json: { count: count }, status: :ok
  end

  private

  # title must not have any \n
  # returns {
  #  translated_title,
  #  translated_text,
  #  source_language
  # }
  def translate_text_title(text, title)
    text_to_be_translated = title ? "#{title}\n#{text}" : text
    translated_text, source_language = translate_text_to_english(
      text_to_be_translated
    ).values_at(
      :translated_text,
      :source_language
    )
    res_title = nil
    res_text = nil
    if source_language != 'en'
      if title
        separation_i = translated_text.index("\n")
        res_title = translated_text[0..separation_i - 1]
        res_text = translated_text[separation_i + 1..]
      else
        res_text = translated_text
      end
    end
    return {
      translated_title: res_title,
      translated_text: res_text,
      source_language: source_language
    }
  end

end
