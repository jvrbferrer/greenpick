# Set of CRUD endpoints for users
class UsersController < ApplicationController
  before_action :admin?, only: %i[create update search]

  def create
    email = params[:email] == '' ? nil : params[:email]
    phone = params[:phone] == '' ? nil : params[:phone]
    name = params[:name]
    password = params[:password]
    signed_up = params[:signedUp]
    send_email = params[:sendEmail]

    if !email && !phone
      return render json: {
        error: 'Number or email is required'
      }, status: :unprocessable_entity
    end

    user = User.new(
      email: email,
      phone: phone,
      name: name,
      password: password,
      signed_up: signed_up,
      is_admin: false
    )
    if user.save
      SubscriptionMailer.with(user: user).subscription_email.deliver if send_email
      return render status: :ok, json: {
        user: user
      }
    else
      return render json: {
        error: user.errors.messages
      }, status: :unprocessable_entity
    end
  end

  def update
    email = params[:email] == '' ? nil : params[:email]
    phone = params[:phone] == '' ? nil : params[:phone]
    name = params[:name]
    signed_up = params[:signedUp]
    id = params[:id]
    password = params[:password]

    user = User.find_by(id: id)
    if !user
      return render json: {
        error: {
          id: 'Id does not exist'
        }
      }, status: :unprocessable_entity
    end

    user.email = email
    user.phone = phone
    user.name = name
    user.password = password
    user.signed_up = signed_up
    user.password = password if password != ''

    updated = user.save

    if updated
      return render status: :ok, json: {
        user: user
      }
    else
      return render json: {
        error: user.errors.messages
      }, status: :unprocessable_entity
    end
  end

  def add_to_mailing_list
    email = params[:email]
    user = User.new(
      email: email,
      phone: nil,
      name: nil,
      password: 'password',
      signed_up: false
    )

    if user.save
      SubscriptionMailer.with(user: user).subscription_email.deliver
      return render status: :ok, json: {
        user: user
      }
    else
      return render json: {
        error: user.errors.messages
      }, status: :unprocessable_entity
    end
  end

  def search
    query_string = params[:query]

    users = User.where('email LIKE ?', "%#{query_string}%").or(
      User.where('name LIKE ?', "%#{query_string}%")
    ).or(
      User.where('phone LIKE ?', "%#{query_string}%")
    ).all

    render json: {
      users: users
    }, status: :ok
  end

  def contact
    ContactMailer.with(email_address: params[:info][:email]).contact_email.deliver
    ContactMailer.with(info: params[:info]).send_to_inbox.deliver
    render status: :ok, json: { message: 'Email sent successfully!' }
  end
end
