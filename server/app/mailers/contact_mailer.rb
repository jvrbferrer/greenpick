class ContactMailer < ApplicationMailer
  def contact_email
    mail(to: params[:email_address], subject: 'Thank you for contacting us!')
  end

  def send_to_inbox
    @name = params[:info][:name]
    @email = params[:info][:email]
    @text = params[:info][:text]
    mail(to: ENV['EMAIL_ADDRESS'], subject: 'New customer message')
  end
end
