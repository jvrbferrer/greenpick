class SubscriptionMailer < ApplicationMailer
  def subscription_email
    @user = params[:user]
    mail(to: @user.email, subject: 'Thank you for subscribing!')
  end
end
