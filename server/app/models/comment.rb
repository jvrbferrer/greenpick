class Comment < ApplicationRecord
  belongs_to :review
  belongs_to :user

  belongs_to :user_public, -> { select(:id, :name) }, class_name: 'User', foreign_key: 'user_id'
  validates :text, presence: true
end
