class Company < ApplicationRecord
  validates :name, presence: true
  validates :country_code, presence: true
  validates :verified, inclusion: { in: [true, false] }
  validates :name, uniqueness: { scope: :country_code }
  validates :website, uniqueness: true, allow_blank: true
  has_many :reviews
end
