# Model with info about each generated prize
class PrizeCode < ApplicationRecord

  validates :code_string, uniqueness: true

  enum prize: %i[tree plastic]
  validates :prize, presence: true

  belongs_to :user, optional: true
end
