class Review < ApplicationRecord
  belongs_to :company
  belongs_to :user
  belongs_to :user_public, -> { select(:id, :name) }, class_name: 'User', foreign_key: 'user_id'
  has_many :review_images
  has_many :comments
end
