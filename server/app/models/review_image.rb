class ReviewImage < ApplicationRecord
  belongs_to :review
  validates :key, presence: true
end
