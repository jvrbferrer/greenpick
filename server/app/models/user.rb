# Model with info about subscribed users and users who have claimed rewards
class User < ApplicationRecord
  has_secure_password
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email,
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: true,
            allow_blank: true
  validates :phone,
            uniqueness: true,
            allow_blank: true
  has_many :prize_codes
  has_many :reviews
  has_many :comments

  def to_token_payload
    # Returns the payload as a hash
    return {
      "sub": self.id,
      "admin": self.is_admin
    }
  end
end
