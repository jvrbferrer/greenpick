Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  post '/prize_codes', to: 'codes#create'
  post '/prize_codes/submit', to: 'codes#submit'
  get '/prize_codes/info', to: 'codes#info_for_code'
  get '/prize_codes/trees/count', to: 'codes#tree_count'
  get '/prize_codes', to: 'codes#index'
  
  post '/users', to: 'users#create'
  post '/users/update', to: 'users#update'
  post '/users/contact', to: 'users#contact'
  get '/users/search', to: 'users#search'
  post '/users/add_to_mailing_list', to: 'users#add_to_mailing_list'
  
  post '/reviews', to: 'reviews#create'
  get '/reviews/count', to: 'reviews#count'
  post 'reviews/delete', to: 'reviews#delete'
  
  post 'companies', to: 'companies#create'
  get 'companies/find', to: 'companies#find'
  get 'companies/reviews', to: 'companies#reviews'
  get 'companies/search_name', to: 'companies#search_name'
  post 'companies/delete', to: 'companies#delete'
  post 'companies/update', to: 'companies#update'

  post 'comments', to: 'comments#create'

  post 'user_token' => 'user_token#create'
end
