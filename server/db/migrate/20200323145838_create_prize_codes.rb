class CreatePrizeCodes < ActiveRecord::Migration[6.0]
  def change
    create_table :prize_codes do |t|
      t.string :code_string, null: false
      t.integer :prize, null: false
      t.boolean :used, default: false, null: false
      t.timestamps
    end
  end
end
