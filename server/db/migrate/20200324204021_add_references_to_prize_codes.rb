class AddReferencesToPrizeCodes < ActiveRecord::Migration[6.0]
  def change
    add_reference :prize_codes, :user, null: true, foreign_key: true
  end
end
