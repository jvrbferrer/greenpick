class AddCompanyToReviews < ActiveRecord::Migration[6.0]
  def change
    add_reference :reviews, :company, null: false, foreign_key: true
  end
end
