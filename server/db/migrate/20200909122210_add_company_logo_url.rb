class AddCompanyLogoUrl < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :logo_key, :string
  end
end
