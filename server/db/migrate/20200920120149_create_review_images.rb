class CreateReviewImages < ActiveRecord::Migration[6.0]
  def change
    create_table :review_images do |t|
      t.string :key
      t.references :review, null: false, foreign_key: true

      t.timestamps
    end
  end
end
