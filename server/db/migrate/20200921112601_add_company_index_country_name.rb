class AddCompanyIndexCountryName < ActiveRecord::Migration[6.0]
  def change
    add_index :companies, [:name, :country_code], unique: true
  end
end
