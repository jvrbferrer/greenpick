class AddReviewToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :reviews, :user, index: true, foreign_key: true, null: false
    change_column_null :users, :email, true
    add_column :users, :phone, :string
    add_column :users, :signed_up, :boolean, null: false, default: false
  end
end
