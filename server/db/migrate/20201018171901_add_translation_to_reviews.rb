class AddTranslationToReviews < ActiveRecord::Migration[6.0]
  def change
    add_column :reviews, :text_en, :string
    add_column :reviews, :original_lang, :string
  end
end
