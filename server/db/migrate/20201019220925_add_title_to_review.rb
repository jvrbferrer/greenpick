class AddTitleToReview < ActiveRecord::Migration[6.0]
  def change
    add_column :reviews, :title, :string
    add_column :reviews, :title_en, :string
  end
end
